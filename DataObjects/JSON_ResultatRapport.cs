﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValgResults.DataObjects
{
    public class Resultatrapport
    {
        [JsonProperty("id")]
        public RapportId rapportId { get; set; }
        public Tidspunkt tidspunkt { get; set; }
        public List<int> merknader { get; set; }

        public int antallsb { get; set; }
        public Mandater mandater { get; set; }

        [JsonProperty("stemmer")]
        public Stemmesedler stemmesedler { get; set; }

        public Stemmegiving stemmegiving { get; set; }
        public Frammote frammote { get; set; }

        public int StatusIndikator { get; set; }
        public bool ShouldSerializeStatusIndikator()
        {
            return StatusIndikator > 0;
        }

        public string LegacySTRegion { get; set; }
        public bool ShouldSerializeLegacySTRegion()
        {
            return ! string.IsNullOrWhiteSpace(LegacySTRegion);
        }

        public Opptalt opptalt { get; set; }
        public Prognose prognose { get; set; }
        public bool ShouldSerializeprognose()
        {
            return prognose != null;
        }

        public Sammenlignbar sammenlignbar { get; set; }
        public bool ShouldSerializesammenlignbar()
        {
            return sammenlignbar != null;
        }

        public List<Parti> partier { get; set; }

        [JsonProperty("_links")]
        public Lenker lenker { get; set; }
    }

    public class Relasjon
    {
        [JsonProperty(Order = 1)]
        public string nr { get; set; }
        [JsonProperty(Order = 2)]
        public string href { get; set; }
        [JsonProperty(Order = 3)]
        public string hrefNavn { get; set; }
        [JsonProperty(Order = 4)]
        public string navn { get; set; }
    }

    public class Self : Relasjon
    {
        [JsonProperty(Order = 5)]
        public double forelopigFhs { get; set; }
        [JsonProperty(Order = 6)]
        public double forelopigVts { get; set; }
        [JsonProperty(Order = 7)]
        public double forelopig { get; set; }
        [JsonProperty(Order = 8)]
        public double endelig { get; set; }
        [JsonProperty(Order = 9)]
        public double oppgjor { get; set; }
    }

    public class Subrapport : Self
    {
        [JsonProperty(Order = 10)]
        public DateTime rapportGenerert { get; set; }
        [JsonProperty(Order = 11)]
        public bool harUnderordnet { get; set; }
    }

    public class Lenker
    {
        [JsonProperty("related")]
        public List<Subrapport> subrapporter { get; set; }
        public Self self { get; set; }
        public Relasjon up { get; set; }
    }

    public class RapportId
    {
        public string valgaar { get; set; }
        public string valgtype { get; set; }
        public string nivaa { get; set; }
        public string navn { get; set; }
        public string nr { get; set; }
    }

    public class Tidspunkt
    {
        public DateTime rapportGenerert { get; set; }
        public DateTime? sisteStemmer { get; set; }
    }

    public class Mandater
    {
        public int antall { get; set; }
        public int? endring { get; set; }
    }

    public class Stemmesedler
    {
        public int total { get; set; }
        public int fhs { get; set; }
        public int totalForkastede { get; set; }
        public int fhsForkastede { get; set; }
    }

    public class Stemmegiving
    {
        public int totalGodkjente { get; set; }
        public int fhsGodkjente { get; set; }
        public int totalForkastede { get; set; }
        public int fhsForkastede { get; set; }
    }

    public class Frammote
    {
        public double? prosent { get; set; }
        public double? endring { get; set; }
        public double? prosentStemmegiving { get; set; }
    }

    public class Opptalt
    {
        public bool outputExtended { get; set; }
        public bool ShouldSerializeoutputExtended()
        { return false; }


        // Fix for missing meaned out opptalt from 2019 election
        // Original formula:
        // Overordnet opptaltprosent. Følger max(opptalt.forlopig, opptalt.endelig) inntil 99.8, deretter opptalt.endelig til 99.9 og 
        //deretter opptalt.oppgjor til 100.0. Unntak for ST-valg på fylkesnivå grunnet utjevningsmandater: Kan bare bli 100.0 dersom landet totalt også er 100.0.         
        public double prosent
        {
            get
            {
                var ret = forelopig;
                if (endelig > forelopig || forelopig >= 99.8) ret = System.Math.Max(endelig,99.8);
                if (ret >= 99.9) ret = System.Math.Max(oppgjor, 99.9);
                return ret;
            }
        }
        public bool ShouldSerializeprosent()
        {
            return outputExtended;
        }

        public double forelopigFhs { get; set; }
        public double forelopigVts { get; set; }
        public double forelopig { get; set; }
        public double endelig { get; set; }
        public double oppgjor { get; set; }
    }

    public class Prognose
    {
        public bool beregnet { get; set; }
        public double? historiskTotalavvik { get; set; }
    }

    public class Sammenlignbar
    {
        public bool samme { get; set; }
        public bool ekvivalent { get; set; }
    }

    //Parti-objekter
    public class Parti
    {
        [JsonProperty("id")]
        public PartiId partiNavn { get; set; }

        [JsonProperty("stemmer")]
        public PartiStemmer stemmetall { get; set; }

        [JsonProperty("mandater")]
        public PartiMandater mandattall { get; set; }
        public bool ShouldSerializemandattall()
        {
            return mandattall != null;
        }
    }

    public class PartiId
    {
        public int partikategori { get; set; }
        public string partikode { get; set; }
        public string navn { get; set; }
    }

    //Stemmetall per parti
    public class PartiStemmer
    {
        public StemmeResultat resultat { get; set; }

        public StemmePrognose prognose { get; set; }
        public bool ShouldSerializeprognose()
        {
            return prognose != null;
        }
    }

    public class Endring
    {
        [JsonConverter(typeof(PadDoubleTo5DecimalsConverter))]
        public double? samme { get; set; }
        [JsonConverter(typeof(PadDoubleTo5DecimalsConverter))]
        public double? ekvivalent { get; set; }
        public bool ShouldSerializeekvivalent()
        {
            return ekvivalent.HasValue;
        }

    }

    public class Antall
    {
        public int total { get; set; }
        public int fhs { get; set; }
    }

    public class StemmeResultat
    {
        [JsonConverter(typeof(PadDoubleTo5DecimalsConverter))]
        public double? prosent { get; set; }

        public Endring endring { get; set; }
        public Antall antall { get; set; }
    }

    public class StemmePrognose
    {
        [JsonConverter(typeof(PadDoubleTo5DecimalsConverter))]
        public double? prosent { get; set; }
        public Endring endring { get; set; }
    }

    //Mandater per parti
    public class PartiMandater
    {
        public MandatResultat resultat { get; set; }
        public bool ShouldSerializeresultat()
        {
            return resultat != null;
        }

        public MandatResultat prognose { get; set; }
        public bool ShouldSerializeprognose()
        {
            return prognose != null;
        }

        //Moved down to resultat/prognose separately
        //public List<Representant> representanter { get; set; }
        //public bool ShouldSerializerepresentanter()
        //{
        //    return representanter != null;
        //}
    }

    public class MandatResultat
    {
        public int? antall { get; set; }
        public int? endring { get; set; }

        public Mandatmargin sisteMandat { get; set; }
        public bool ShouldSerializesisteMandat()
        {
            return sisteMandat != null;
        }

        public Mandatmargin nesteMandat { get; set; }
        public bool ShouldSerializenesteMandat()
        {
            return nesteMandat != null;
        }

        public int? utjevningAntall { get; set; }

        public Mandatmargin utjevning { get; set; }
        public bool ShouldSerializeutjevning()
        {
            return utjevning != null;
        }

        public MandatResultat ekvivalent { get; set; }
        public bool ShouldSerializeekvivalent()
        {
            return ekvivalent != null;
        }

        public List<Representant> representanter { get; set; }
        public bool ShouldSerializerepresentanter()
        {
            return representanter != null;
        }

        public List<Representant> nesteKandidater { get; set; }
        public bool ShouldSerializenesteKandidater()
        {
            return nesteKandidater != null;
        }

    }

    public class Mandatmargin
    {
        [JsonConverter(typeof(PadDoubleTo5DecimalsConverter))]
        public double? kvotient { get; set; }
        public int? mandatrang { get; set; }

    }


    public class Representant
    {
        public string navn { get; set; }
        public string kjonn { get; set; }
        public int alder { get; set; }
        public bool? utjevningsmandat { get; set; }
        public bool ShouldSerializeutjevningsmandat()
        {
            return utjevningsmandat.HasValue;
        }
    }


    internal sealed class PadDoubleTo5DecimalsConverter : JsonConverter
    {
        public override bool CanRead
        {
            get { return false; }
        }

        public override bool CanWrite
        {
            get { return true; }
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(double);
        }

        public override void WriteJson(
            JsonWriter writer, object value, JsonSerializer serializer)
        {
            double number = (double)value;
            writer.WriteRawValue(number.ToString("F5", CultureInfo.InvariantCulture));
        }

        //Read support not needed/supported
        public override object ReadJson(
            JsonReader reader, Type type, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
