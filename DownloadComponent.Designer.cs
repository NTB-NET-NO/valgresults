﻿namespace ValgResults
{
    partial class DownloadComponent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pollTimer = new System.Timers.Timer();
            ((System.ComponentModel.ISupportInitialize)(this.pollTimer)).BeginInit();
            // 
            // pollTimer
            // 
            this.pollTimer.Interval = 30000D;
            this.pollTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.pollTimer_Elapsed);
            ((System.ComponentModel.ISupportInitialize)(this.pollTimer)).EndInit();

        }

        #endregion

        private System.Timers.Timer pollTimer;
    }
}
