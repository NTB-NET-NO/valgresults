﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using ValgResults.Configuration;

namespace ValgResults
{
    public partial class ValgResultsService : ServiceBase
    {
        /// <summary>
        /// Logger
        /// </summary>
        static ILog logger = LogManager.GetLogger(typeof(ValgResultsService));

        /// <summary>
        /// The actual main work object
        /// </summary>
        protected Dictionary<string, DownloadComponent> workers = new Dictionary<string, DownloadComponent>();

        public ValgResultsService()
        {
            log4net.Config.XmlConfigurator.Configure();
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                logger.Info("ValgResultsService SERVICEMODE starting...");

                GlobalConfig.LoadConfig();
                ComponentConfigSection configSection =
                ConfigurationManager.GetSection("ValgComponentConfigSection") as ComponentConfigSection;

                foreach (ValgComponentConfiguration item in configSection.ValgDownloaders)
                {
                    if (item.Enabled)
                    {
                        var w = new DownloadComponent();
                        w.Configure(item);
                        workers.Add(item.Name, w);
                    }
                }

                workers.Values.ToList().ForEach(w => w.Start());
                logger.Info("ValgResultsService SERVICEMODE started");
            }
            catch (Exception ex)
            {
                logger.Error("ValgResultsService SERVICEMODE DID NOT START properly", ex);
                //workers.queryObject.Stop();
            }
        }

        protected override void OnStop()
        {

            try
            {
                logger.Info("ValgResultsService SERVICEMODE stopping...");
                workers.Values.ToList().ForEach(w => w.Stop());
                logger.Info("ValgResultsService SERVICEMODE stopped");
            }
            catch (Exception ex)
            {
                logger.Error("ValgResultsService SERVICEMODE DID NOT STOP properly", ex);
            }

        }
    }
}
