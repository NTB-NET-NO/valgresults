﻿using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ValgResults.Configuration;
using ValgResults.DataObjects;

namespace ValgResults
{
    public partial class DownloadComponent : Component
    {
        /// <summary>
        /// Log object
        /// </summary>
        static ILog logger = LogManager.GetLogger(typeof(DownloadComponent));

        #region System data

        /// <summary>
        /// Exit control event
        /// </summary>
        protected System.Threading.AutoResetEvent _busyEvent = new System.Threading.AutoResetEvent(true);

        /// <summary>
        /// Report cache, used for getting aggregated data for the XML reports
        /// </summary>
        protected Dictionary<string, Resultatrapport> reportCache = new Dictionary<string, Resultatrapport>();

        /// <summary>
        /// XML ResponsRapport cache, used for creating aggregated XML reports.
        /// Not saved to disk between runs but rather rebuilt when empty.
        /// </summary>
        protected Dictionary<string, responsRapport> XMLCache = new Dictionary<string, responsRapport>();

        /// <summary>
        /// Progress cache, used for tracking progress thru last-update timestamps
        /// </summary>
        protected Dictionary<string, DateTime> progressCache = new Dictionary<string, DateTime>();

        protected ValgComponentConfiguration config = null;

        #endregion

        //Constructors
        static DownloadComponent()
        {
            //Set up logger
            log4net.Config.XmlConfigurator.Configure();
            if (!log4net.LogManager.GetRepository().Configured)
                log4net.Config.BasicConfigurator.Configure();
        }

        public DownloadComponent()
        {
            InitializeComponent();
        }

        public DownloadComponent(IContainer container)
        {
            container.Add(this);
            InitializeComponent();
        }

        /// <summary>
        /// Configures this instance.
        /// </summary>
        public void Configure(ValgComponentConfiguration conf)
        {
            //Basic config
            config = conf;
            NDC.Push(config.Name);

            logger.InfoFormat("ValgComponent loaded. Valgtype: {0} / ReportRoot: {1}", config.ValgType, config.ReportRoot);
            logger.DebugFormat("PollInterval: {0}", config.PollInterval);
            logger.DebugFormat("DataFolder: {0}", config.DataFolder);
            logger.DebugFormat("JSONOutputFolder: {0}", config.JSONOutputFolder);
            logger.DebugFormat("XMLOutputFolder: {0}", config.XMLOutputFolder);
            logger.DebugFormat("XMLReportTypeFolders: {0}", config.XMLReportTypeFolders);
            logger.DebugFormat("DisableXMLReports: {0}", config.DisableXMLReports);

            //Load Caches
            try
            {
                LoadCache();
            }
            catch (Exception ex)
            {
                logger.Warn("Failed to load cache. Downloading data from scratch.", ex);
                reportCache.Clear();
                progressCache.Clear();
            }

            //Flag Config OK
            NDC.Pop();
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public void Start()
        {
            //Check status
            if (config == null)
                throw new NotSupportedException("DownloadComponent is not properly configured. DownloadComponent::Start() aborted");

            _busyEvent.Set();
            pollTimer.Interval = config.PollInterval * 1000;
            pollTimer.Start();

        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        public void Stop()
        {
            NDC.Push(config.Name);

            if (!pollTimer.Enabled)
                logger.Info("Waiting for current update sequence to complete.");

            if (!_busyEvent.WaitOne(20000, false))
                logger.Warn("Last update sequence did not complete properly. Valgdata-files may be inconsistent.");

            try
            {
                SaveCache();
            }
            catch (Exception ex)
            {
                logger.Warn("Failed to save CACHE.", ex);
            }

            pollTimer.Stop();
            NDC.Pop();
        }

        private void pollTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            NDC.Push(config.Name);
            logger.DebugFormat("pollTimer_Elapsed hit...");

            pollTimer.Stop();
            _busyEvent.WaitOne();

            try
            {
                //Run the update
                var updates = UpdateResultReport(config.ReportRoot);

                //Perform some extra task when changes are detected.
                if (updates)
                {
                    //Save extra JSON progress
                    if (GlobalConfig.CreateJSONProgress)
                        SaveProgressJSON();

                    //Save extra JSON aggregates
                    if (GlobalConfig.CreateJSONAggregates)
                    {
                        SaveAggregatedJSON_Fylker();
                        SaveAggregatedJSON_Extra();
                    }
                    //Save current cache/progress to disk
                    SaveCache();
                }

                if (updates && !config.DisableXMLReports)
                {
                    //Generate aggregated XML reports - xx03/04/05 and xx07
                    switch (config.ValgType)
                    {
                        case ValgTyper.ST:
                            SaveAggregatedXML_ST();
                            break;
                        case ValgTyper.F:
                            SaveAggregatedXML_F();
                            break;
                        case ValgTyper.K:
                            SaveAggregatedXML_K();
                            break;
                        case ValgTyper.SA:
                            SaveAggregatedXML_SA();
                            break;
                        default:
                            break;
                    }
                }

            }
            catch (Exception ex)
            {
                logger.Error("JSON Update sequence failed.", ex);

                //Force at least st-root on the next run
                progressCache.Remove(config.ReportRoot);
            }

            _busyEvent.Set();
            pollTimer.Start();
            NDC.Pop();

        }

        protected bool UpdateResultReport(string reportString)
        {
            //var status1 = reportCache.Values.Where(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && Resultatrapport.StatusIndikator < 8);
            //var status2 = reportCache.Values.Where(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.StatusIndikator < 8);

            //Fetch report
            string json = Helpers.Tools.DoQuery(reportString);
            var report = JsonConvert.DeserializeObject<Resultatrapport>(json);

            //Special case for root-landsoversikt, must be checked for changes here, no need to dump an unchanged root report all the time
            if (progressCache.ContainsKey(report.lenker.self.href) && report.tidspunkt.rapportGenerert <= progressCache[report.lenker.self.href])
            {
                logger.InfoFormat("Unchanged Report skipped: {0}-{1}", report.lenker.self.href.Replace("/", "-").TrimStart('-').ToUpper(), report.lenker.self.navn);
                return false;
            }

            //Dump
            var idstring = Helpers.Tools.BuildIdString(report);
            report.opptalt.outputExtended = true;
            Helpers.Tools.DumpToFile(JsonConvert.SerializeObject(report), config.JSONOutputFolder, string.Format("{0}.json", Helpers.Tools.BuildFilename(report)));

            //Verify model using content cleaned of whitespace
            report.opptalt.outputExtended = false;
            var json2 = JsonConvert.SerializeObject(report);
            if (Regex.Replace(json, @"\s", "") != Regex.Replace(json2, @"\s", ""))
            {
                logger.WarnFormat("Report JSON MISMATCH after serialization: {0}", idstring);

                Helpers.Tools.DumpToFile(json, Path.Combine(config.DataFolder, "debug"), string.Format("{0}.before.json", idstring));
                Helpers.Tools.DumpToFile(json2, Path.Combine(config.DataFolder, "debug"), string.Format("{0}.after.json", idstring));
            }

            //Set calculated StatusIndikator on all leaf-node reports
            if (!report.lenker.subrapporter.Any())
                report.StatusIndikator = Helpers.Tools.GetStatusIndicator(report);

            //Identify kommune/krets and set parent legacy region number as temp field
            if (report.rapportId.nivaa == "kommune")
                report.LegacySTRegion = report.lenker.up.nr;
            else if (report.rapportId.nivaa == "stemmekrets")
                report.LegacySTRegion = reportCache[report.lenker.up.href].lenker.up.nr;

            //Update internal report cache before sub-ing into children, complete parent set needed for XML generation
            reportCache[reportString] = report;

            //TODO: ST2025 - We need update all FY-reports when one changes to account for the ever changing utjevnings-mandater 
            //TODO: ST2025 - This shoudl not be necessary based on the generated timestamps?
            //TODO: ST2025 - This shoudl not be necessary based on the generated timestamps?
            //Loop subreports, update on missing/changes only
            foreach (Subrapport item in report.lenker.subrapporter)
            {
                if (!progressCache.ContainsKey(item.href) || item.rapportGenerert > progressCache[item.href])
                    UpdateResultReport(item.href);
            }

            //Dump an aggregated county JSON file if we're at county level
            if (report.rapportId.nivaa == "fylke" && GlobalConfig.CreateJSONAggregates)
                SaveAggregatedJSON_Kommuner(reportString);

            //Save XML after all caches have been updated
            if (!config.DisableXMLReports) SaveConvertedXML(report);

            //Log report completion on this level`and update progressa
            progressCache[report.lenker.self.href] = report.tidspunkt.rapportGenerert;
            logger.InfoFormat("Report updated: {0}", idstring);

            return true;
        }

        protected void SaveAggregatedXML_ST()
        {
            //Prognoseoversikt ST07
            respons xmlReport = Helpers.XmlHelpersST.GetST07(reportCache, config.ReportRoot);
            xmlReport.SaveToFile(Path.Combine(config.XMLOutputFolder, "ST07.xml"), Encoding.GetEncoding("iso-8859-1"));
            logger.Info("Aggregated XML-report generated: ST07-Prognoseoversikt");

            //Oppmøteoversikt ST14
            xmlReport = Helpers.XmlHelpersST.GetST14(reportCache, config.ReportRoot);
            xmlReport.SaveToFile(Path.Combine(config.XMLOutputFolder, "ST14.xml"), Encoding.GetEncoding("iso-8859-1"));
            logger.Info("Aggregated XML-report generated: ST14-Oppmøteoversikt");

            if (!XMLCache.Any())
            {
                logger.Info("XML rapport cache is empty, rebuilding...");
                RebuildXMLCache();
            }

            //Aggregated ST03            
            var st03 = new respons();
            foreach (var r in XMLCache.Where(x => x.Key.StartsWith("ST03")).OrderBy(o => o.Key).Select(y => y.Value))
                st03.rapport.Add(r);
            st03.SaveToFile(Path.Combine(config.XMLOutputFolder, "ST03.xml"), Encoding.GetEncoding("iso-8859-1"));
            logger.InfoFormat("Aggregated XML-report generated: ST03-Aggregated, {0} items", st03.rapport.Count);

            //Aggregated ST04            
            var st04 = new respons();
            foreach (var r in XMLCache.Where(x => x.Key.StartsWith("ST04")).Select(y => y.Value))
                st04.rapport.Add(r);
            st04.SaveToFile(Path.Combine(config.XMLOutputFolder, "ST04.xml"), Encoding.GetEncoding("iso-8859-1"));
            logger.InfoFormat("Aggregated XML-report generated: ST04-Aggregated, {0} items", st04.rapport.Count);

            //Aggregated ST05            
            var st05 = new respons();
            foreach (var r in XMLCache.Where(x => x.Key.StartsWith("ST05")).OrderBy(o => o.Key).Select(y => y.Value))
                st05.rapport.Add(r);
            st05.SaveToFile(Path.Combine(config.XMLOutputFolder, "ST05.xml"), Encoding.GetEncoding("iso-8859-1"));
            logger.InfoFormat("Aggregated XML-report generated: ST05-Aggregated, {0} items", st05.rapport.Count);

        }

        protected void SaveAggregatedXML_F()
        {
            //Prognoseoversikt F07
            respons xmlReport = Helpers.XmlHelpersFY.GetF07(reportCache, config.ReportRoot);
            xmlReport.SaveToFile(Path.Combine(config.XMLOutputFolder, "F07.xml"), Encoding.GetEncoding("iso-8859-1"));
            logger.Info("Aggregated XML-report generated: F07-Prognoseoversikt");

            //Progress K99 - Custom XML progress list
            if (GlobalConfig.CreateXMLProgress)
            {
                xmlReport = Helpers.XmlHelpers.GetP99(reportCache, "F");
                xmlReport.SaveToFile(Path.Combine(config.XMLOutputFolder, "F99.xml"), Encoding.GetEncoding("iso-8859-1"));
                logger.Info("Aggregated XML-report generated: F99-Custom progress");
            }
            if (!XMLCache.Any())
            {
                logger.Info("XML rapport cache is empty, rebuilding...");
                RebuildXMLCache();
            }

            //Aggregated F01           
            var f01 = new respons();
            foreach (var r in XMLCache.Where(x => x.Key.StartsWith("F01")).Select(y => y.Value))
                f01.rapport.Add(r);
            f01.SaveToFile(Path.Combine(config.XMLOutputFolder, "F01.xml"), Encoding.GetEncoding("iso-8859-1"));
            logger.InfoFormat("Aggregated XML-report generated: F01-Aggregated, {0} items", f01.rapport.Count);

            //Aggregated F03         
            var f03 = new respons();
            foreach (var r in XMLCache.Where(x => x.Key.StartsWith("F03")).OrderBy(o => o.Key).Select(y => y.Value))
                f03.rapport.Add(r);
            f03.SaveToFile(Path.Combine(config.XMLOutputFolder, "F03.xml"), Encoding.GetEncoding("iso-8859-1"));
            logger.InfoFormat("Aggregated XML-report generated: F03-Aggregated, {0} items", f03.rapport.Count);

            //Aggregated F04            
            var f04 = new respons();
            foreach (var r in XMLCache.Where(x => x.Key.StartsWith("F04")).Select(y => y.Value))
                f04.rapport.Add(r);
            f04.SaveToFile(Path.Combine(config.XMLOutputFolder, "F04.xml"), Encoding.GetEncoding("iso-8859-1"));
            logger.InfoFormat("Aggregated XML-report generated: F04-Aggregated, {0} items", f04.rapport.Count);

        }

        protected void SaveAggregatedXML_K()
        {
            //Fremøteoversikt K07
            respons xmlReport = Helpers.XmlHelpersKO.GetK07(reportCache, config.ReportRoot);
            xmlReport.SaveToFile(Path.Combine(config.XMLOutputFolder, "K07.xml"), Encoding.GetEncoding("iso-8859-1"));
            logger.Info("Aggregated XML-report generated: K07-Fremøteoversikt");

            //Bystyreoversikt K09 - Hardcode Oslo here
            xmlReport = Helpers.XmlHelpersKO.GetK09(reportCache, config.ReportRoot + "/03/0301");
            xmlReport.SaveToFile(Path.Combine(config.XMLOutputFolder, "K09.xml"), Encoding.GetEncoding("iso-8859-1"));
            logger.Info("Aggregated XML-report generated: K09-Bystyreoversikt");

            //Progress K99 - Custom XML progress list
            if (GlobalConfig.CreateXMLProgress)
            {
                xmlReport = Helpers.XmlHelpers.GetP99(reportCache, "K");
                xmlReport.SaveToFile(Path.Combine(config.XMLOutputFolder, "K99.xml"), Encoding.GetEncoding("iso-8859-1"));
                logger.Info("Aggregated XML-report generated: K99-Custom progress");
            }

            if (!XMLCache.Any())
            {
                logger.Info("XML rapport cache is empty, rebuilding...");
                RebuildXMLCache();
            }

            //Aggregated K01           
            var k01 = new respons();
            foreach (var r in XMLCache.Where(x => x.Key.StartsWith("K01")).Select(y => y.Value))
                k01.rapport.Add(r);
            k01.SaveToFile(Path.Combine(config.XMLOutputFolder, "K01.xml"), Encoding.GetEncoding("iso-8859-1"));
            logger.InfoFormat("Aggregated XML-report generated: K01-Aggregated, {0} items", k01.rapport.Count);

            //Aggregated K03         
            var k03 = new respons();
            foreach (var r in XMLCache.Where(x => x.Key.StartsWith("K03")).OrderBy(o => o.Key).Select(y => y.Value))
                k03.rapport.Add(r);
            k03.SaveToFile(Path.Combine(config.XMLOutputFolder, "K03.xml"), Encoding.GetEncoding("iso-8859-1"));
            logger.InfoFormat("Aggregated XML-report generated: K03-Aggregated, {0} items", k03.rapport.Count);

            //Aggregated K04            
            var k04 = new respons();
            foreach (var r in XMLCache.Where(x => x.Key.StartsWith("k04")).Select(y => y.Value))
                k04.rapport.Add(r);
            k04.SaveToFile(Path.Combine(config.XMLOutputFolder, "K04.xml"), Encoding.GetEncoding("iso-8859-1"));
            logger.InfoFormat("Aggregated XML-report generated: K04-Aggregated, {0} items", k04.rapport.Count);

            //Aggregated K08       
            var k08 = new respons();
            foreach (var r in XMLCache.Where(x => x.Key.StartsWith("K08")).OrderBy(y => y.Key).Select(z => z.Value))
                k08.rapport.Add(r);
            k08.SaveToFile(Path.Combine(config.XMLOutputFolder, "K08.xml"), Encoding.GetEncoding("iso-8859-1"));
            logger.InfoFormat("Aggregated XML-report generated: K08-Aggregated, {0} items", k08.rapport.Count);
        }

        protected void SaveAggregatedXML_SA()
        {
            throw new NotImplementedException("XML conversion not implemented for this election type: SA");
        }

        protected void SaveProgressJSON()
        {
            //Start by gathering lists of JSON reports per kommune, ordered by report date, descending
            var kommuner = reportCache.Values.Where(r => r.rapportId.nivaa == "kommune").OrderByDescending(x => x.tidspunkt.rapportGenerert);

            //Build a JSON list dynamically, taking minimal info from cache items
            JArray progressList = new JArray();
            foreach (var k in kommuner)
            {
                dynamic p = new JObject();
                p.valgType = k.rapportId.valgtype;
                p.valgAr = k.rapportId.valgaar;
                p.fylkeNr = k.lenker.up.nr;
                p.kommuneNr = k.rapportId.nr;
                p.kommuneNavn = k.rapportId.navn;
                p.rapportTidspunkt = k.tidspunkt.rapportGenerert;
                progressList.Add(p);
            };

            //Save JSON progress report
            var filename = config.ReportRoot.Replace("/", "-").TrimStart('-').ToUpper() + "-progress.json";
            Helpers.Tools.DumpToFile(progressList.ToString(Formatting.None, null), config.JSONOutputFolder, filename);
            logger.Info("Aggregated JSON-report generated: " + filename);

            //Start by gathering lists of JSON reports per krets, ordered by report date, descending
            var kretser = reportCache.Values.Where(r => r.rapportId.nivaa == "stemmekrets").OrderByDescending(x => x.tidspunkt.rapportGenerert);

            //Build a JSON list dynamically, taking minimal info from cache items
            progressList = new JArray();
            foreach (var k in kretser)
            {
                dynamic p = new JObject();
                p.valgType = k.rapportId.valgtype;
                p.valgAr = k.rapportId.valgaar;

                //Needd to handle Oslo as bydeler is inn between here
                if (k.lenker.self.href.Contains("0301"))
                {
                    p.fylkenr = "03"; 
                    p.kommuneNr = "0301";
                    p.kommuneNavn = "Oslo";
                }
                else
                {
                    p.fylkenr = reportCache[k.lenker.up.href].lenker.up.nr;
                    p.kommuneNr = k.lenker.up.nr;
                    p.kommuneNavn = k.lenker.up.navn;
                }
                
                p.kretsNr = k.rapportId.nr;
                p.kretsNavn = k.rapportId.navn;
                p.rapportTidspunkt = k.tidspunkt.rapportGenerert;
                progressList.Add(p);
            };

            //Save JSON progress report
            filename = config.ReportRoot.Replace("/", "-").TrimStart('-').ToUpper() + "-progress-krets.json";
            Helpers.Tools.DumpToFile(progressList.ToString(Formatting.None, null), config.JSONOutputFolder, filename);
            logger.Info("Aggregated JSON-report generated: " + filename);

            //Build a JSON list dynamically, taking key info from cache items
            var subset = kommuner.Take(GlobalConfig.JSONProgressDataCap);
            progressList = new JArray();
            foreach (var k in subset)
            {
                dynamic p = new JObject();
                p.id = JObject.FromObject(k.rapportId);
                p.tidspunkt = JObject.FromObject(k.tidspunkt);
                p.antallsb = k.antallsb;
                p.stemmer = JObject.FromObject(k.stemmesedler);
                p.stemmegiving = JObject.FromObject(k.stemmegiving);
                p.frammote = JObject.FromObject(k.frammote);
                p.opptalt = JObject.FromObject(k.opptalt);
                //p.prognose = JObject.FromObject(k.prognose);

                p.partier = JArray.FromObject(k.partier.OrderByDescending(x => x.stemmetall.resultat.prosent).Select(y => JObject.FromObject(y)));

                progressList.Add(p);
            };

            //Save JSON progress report
            filename = config.ReportRoot.Replace("/", "-").TrimStart('-').ToUpper() + "-progress-data.json";
            Helpers.Tools.DumpToFile(progressList.ToString(Formatting.None, null), config.JSONOutputFolder, filename);
            logger.Info("Aggregated JSON-report generated: " + filename);
        }

        protected void SaveAggregatedJSON_Fylker()
        {
            //Start by gathering lists of JSON reports per fylke, ordered by report date, descending
            var fylker = reportCache.Values.Where(r => r.rapportId.nivaa == "fylke").OrderByDescending(x => x.tidspunkt.rapportGenerert);

            //Build a JSON list dynamically, taking key info from cache items
            JArray fylkesList = new JArray();
            foreach (var f in fylker)
            {
                dynamic p = new JObject();
                p.id = JObject.FromObject(f.rapportId);
                p.tidspunkt = JObject.FromObject(f.tidspunkt);
                p.antallsb = f.antallsb;
                p.stemmer = JObject.FromObject(f.stemmesedler);
                //p.stemmegiving = JObject.FromObject(k.stemmegiving);
                //p.frammote = JObject.FromObject(k.frammote);
                //p.opptalt = JObject.FromObject(k.opptalt);
                //p.prognose = JObject.FromObject(f.prognose);

                p.partier = JArray.FromObject(f.partier.OrderByDescending(x => x.stemmetall.resultat.prosent).Select(y => JObject.FromObject(y)));
                //p.partier = JArray.FromObject(f.partier);
                fylkesList.Add(p);
            };

            //Save JSON progress report
            var filename = config.ReportRoot.Replace("/", "-").TrimStart('-').ToUpper() + "-fylker-aggregate.json";
            Helpers.Tools.DumpToFile(fylkesList.ToString(Formatting.None, null), config.JSONOutputFolder, filename);
            logger.Info("Aggregated JSON-report generated: " + filename);
        }

        protected void SaveAggregatedJSON_Kommuner(string fylke_rootref)
        {
            //Start by gathering lists of JSON reports per kommune
            var fylke = reportCache[fylke_rootref];
            var kommuner = reportCache.Values.Where(r => r.rapportId.nivaa == "kommune" && r.LegacySTRegion == fylke.rapportId.nr);

            //Build a JSON list dynamically, taking key info from cache items
            JArray countyList = new JArray();
            foreach (var k in kommuner)
            {
                dynamic p = new JObject();
                p.id = JObject.FromObject(k.rapportId);
                p.tidspunkt = JObject.FromObject(k.tidspunkt);
                p.antallsb = k.antallsb;
                p.stemmer = JObject.FromObject(k.stemmesedler);
                //p.stemmegiving = JObject.FromObject(k.stemmegiving);
                //p.frammote = JObject.FromObject(k.frammote);
                //p.opptalt = JObject.FromObject(k.opptalt);
                //p.prognose = JObject.FromObject(k.prognose);

                p.partier = JArray.FromObject(k.partier.OrderByDescending(x => x.stemmetall.resultat.prosent).Select(y => JObject.FromObject(y)));
                countyList.Add(p);
            };

            //Create main object
            dynamic f = new JObject();
            f.id = JObject.FromObject(fylke.rapportId);
            f.kommuner = countyList;

            //Save JSON object
            var filename = string.Format("{0}-{1}-{2}-aggregate.json", config.ReportRoot.Replace("/", "-").TrimStart('-').ToUpper(), fylke.rapportId.nr, "kommuner");
            Helpers.Tools.DumpToFile(f.ToString(Formatting.None, null), config.JSONOutputFolder, filename);
            logger.Info("Aggregated JSON-report generated: " + filename);

        }

        protected void SaveAggregatedJSON_Extra()
        {
            //Start by gathering lists of JSON reports per kommune, ordered by report date, descending
            var kommuner = reportCache.Values.Where(r => r.rapportId.nivaa == "kommune" && r.partier.Any(p => p.partiNavn.partikode == "A"))
                .OrderByDescending(x => x.partier.First(y => y.partiNavn.partikode == "A").stemmetall.resultat.endring.samme)
                .TakeWhile(z => z.partier.First(q => q.partiNavn.partikode == "A").stemmetall.resultat.endring.samme > 0);


            //Build a JSON list dynamically, taking minimal info from cache items
            JArray progressList = new JArray();
            foreach (var k in kommuner)
            {
                dynamic p = new JObject();
                p.valgType = k.rapportId.valgtype;
                p.valgAr = k.rapportId.valgaar;
                p.fylkeNr = k.lenker.up.nr;
                p.kommuneNr = k.rapportId.nr;
                p.kommuneNavn = k.rapportId.navn;
                p.rapportTidspunkt = k.tidspunkt.rapportGenerert;
                p.AP_oppslutning = k.partier.First(x => x.partiNavn.partikode == "A").stemmetall.resultat.prosent;
                p.AP_endring = k.partier.First(x => x.partiNavn.partikode == "A").stemmetall.resultat.endring.samme;
                progressList.Add(p);
            };

            //Save JSON progress report
            var filename = config.ReportRoot.Replace("/", "-").TrimStart('-').ToUpper() + "-AP_fremgang.json";
            Helpers.Tools.DumpToFile(progressList.ToString(Formatting.None, null), config.JSONOutputFolder, filename);
            logger.Info("Aggregated JSON-report generated: " + filename);
        }

        protected void SaveConvertedXML(Resultatrapport report)
        {
            string filename = string.Empty;
            string id = string.Empty;
            respons xmlReport = null;

            //First we switch on election type
            switch (config.ValgType)
            {
                case ValgTyper.ST:

                    //Then switch on level
                    switch (report.rapportId.nivaa)
                    {
                        case "land":
                            xmlReport = Helpers.XmlHelpersST.GetST06(report, reportCache);
                            filename = "ST06.xml";
                            break;
                        case "fylke":
                            id = string.Format("ST04-{0}", report.rapportId.nr);
                            filename = (config.XMLReportTypeFolders ? string.Format("ST04\\{0}.xml", id) : string.Format("{0}.xml", id));

                            xmlReport = Helpers.XmlHelpersST.GetST04(report, reportCache);
                            if (XMLCache.Any())
                                XMLCache[id] = xmlReport.firstRapport;

                            break;
                        case "kommune":
                            xmlReport = Helpers.XmlHelpersST.GetST02(report, reportCache);
                            filename = (config.XMLReportTypeFolders ? string.Format("ST02\\ST02-{0}.xml", report.rapportId.nr) :
                                string.Format("ST02-{0}.xml", report.rapportId.nr));
                            break;
                        case "bydel":
                            id = string.Format("ST05-{0}", report.rapportId.nr.PadLeft(2, '0'));
                            filename = (config.XMLReportTypeFolders ? string.Format("ST05\\ST05-{0}.xml", report.rapportId.nr) :
                                string.Format("ST05-{0}.xml", report.rapportId.nr));

                            xmlReport = Helpers.XmlHelpersST.GetST05(report, reportCache);
                            if (XMLCache.Any())
                                XMLCache[id] = xmlReport.firstRapport;

                            break;
                        case "stemmekrets":
                            id = string.Format("ST03-{0}-{1}",
                                 (reportCache[report.lenker.up.href].rapportId.nivaa == "bydel" ? reportCache[report.lenker.up.href].lenker.up.nr : report.lenker.up.nr),
                                report.rapportId.nr);
                            filename = (config.XMLReportTypeFolders ? string.Format("ST03\\{0}.xml", id) : string.Format("{0}.xml", id));

                            xmlReport = Helpers.XmlHelpersST.GetST03(report, reportCache);
                            if (XMLCache.Any())
                                XMLCache[id] = xmlReport.firstRapport;
                            break;
                    }
                    break;

                case ValgTyper.F:
                    //Then switch on level
                    switch (report.rapportId.nivaa)
                    {
                        case "land":
                            xmlReport = Helpers.XmlHelpersFY.GetF05(report, reportCache);
                            filename = "F05.xml";
                            break;
                        case "fylke":

                            //FY Fylke is split in two reports. Progress first.
                            id = string.Format("F01-{0}", report.rapportId.nr);
                            filename = (config.XMLReportTypeFolders ? string.Format("F01\\{0}.xml", id) : string.Format("{0}.xml", id));

                            xmlReport = Helpers.XmlHelpersFY.GetF01(report, reportCache);
                            if (XMLCache.Any())
                                XMLCache[id] = xmlReport.firstRapport;
                            WriteConvertedXML(xmlReport, filename);

                            //Then move to the actuall results
                            id = string.Format("F04-{0}", report.rapportId.nr);
                            filename = (config.XMLReportTypeFolders ? string.Format("F04\\{0}.xml", id) : string.Format("{0}.xml", id));

                            xmlReport = Helpers.XmlHelpersFY.GetF04(report, reportCache);
                            if (XMLCache.Any())
                                XMLCache[id] = xmlReport.firstRapport;

                            break;
                        case "kommune":
                            xmlReport = Helpers.XmlHelpersFY.GetF02(report, reportCache);
                            filename = (config.XMLReportTypeFolders ? string.Format("F02\\F02-{0}.xml", report.rapportId.nr) :
                                string.Format("F02-{0}.xml", report.rapportId.nr));
                            break;
                        case "bydel":
                            //Not applicable for FY valg
                            break;
                        case "stemmekrets":
                            id = string.Format("F03-{0}-{1}",
                                 (reportCache[report.lenker.up.href].rapportId.nivaa == "bydel" ? reportCache[report.lenker.up.href].lenker.up.nr : report.lenker.up.nr),
                                report.rapportId.nr);
                            filename = (config.XMLReportTypeFolders ? string.Format("F03\\{0}.xml", id) : string.Format("{0}.xml", id));

                            xmlReport = Helpers.XmlHelpersFY.GetF03(report, reportCache);
                            if (XMLCache.Any())
                                XMLCache[id] = xmlReport.firstRapport;
                            break;
                    }
                    break;
                case ValgTyper.K:
                    //Then switch on level
                    switch (report.rapportId.nivaa)
                    {
                        case "land":
                            xmlReport = Helpers.XmlHelpersKO.GetK05(report, reportCache);
                            filename = "K05.xml";
                            break;
                        case "fylke":

                            //FY Fylke is split in two reports. Progress first.
                            id = string.Format("K01-{0}", report.rapportId.nr);
                            filename = (config.XMLReportTypeFolders ? string.Format("K01\\{0}.xml", id) : string.Format("{0}.xml", id));

                            xmlReport = Helpers.XmlHelpersKO.GetK01(report, reportCache);
                            if (XMLCache.Any())
                                XMLCache[id] = xmlReport.firstRapport;

                            WriteConvertedXML(xmlReport, filename);

                            //Then move to the actuall results
                            id = string.Format("K04-{0}", report.rapportId.nr);
                            filename = (config.XMLReportTypeFolders ? string.Format("K04\\{0}.xml", id) : string.Format("{0}.xml", id));

                            xmlReport = Helpers.XmlHelpersKO.GetK04(report, reportCache);
                            if (XMLCache.Any())
                                XMLCache[id] = xmlReport.firstRapport;

                            break;
                        case "kommune":
                            xmlReport = Helpers.XmlHelpersKO.GetK02(report, reportCache);
                            filename = (config.XMLReportTypeFolders ? string.Format("K02\\K02-{0}.xml", report.rapportId.nr) :
                                string.Format("K02-{0}.xml", report.rapportId.nr));
                            break;
                        case "bydel":
                            id = string.Format("K08-{0}", report.rapportId.nr.PadLeft(2, '0'));
                            filename = (config.XMLReportTypeFolders ? string.Format("K08\\{0}.xml", id) :
                                string.Format("{0}.xml", id));

                            xmlReport = Helpers.XmlHelpersKO.GetK08(report, reportCache);
                            if (XMLCache.Any())
                                XMLCache[id] = xmlReport.firstRapport;

                            break;
                        case "stemmekrets":
                            id = string.Format("K03-{0}-{1}",
                                 (reportCache[report.lenker.up.href].rapportId.nivaa == "bydel" ? reportCache[report.lenker.up.href].lenker.up.nr : report.lenker.up.nr),
                                report.rapportId.nr);
                            filename = (config.XMLReportTypeFolders ? string.Format("K03\\{0}.xml", id) : string.Format("{0}.xml", id));

                            xmlReport = Helpers.XmlHelpersKO.GetK03(report, reportCache);
                            if (XMLCache.Any())
                                XMLCache[id] = xmlReport.firstRapport;
                            break;
                    }
                    break;
                case ValgTyper.SA:
                default:
                    throw new NotImplementedException("XML conversion not implemented for this election type: " + config.ValgType);
            }

            WriteConvertedXML(xmlReport, filename);
        }

        protected void WriteConvertedXML(respons xmlReport, string filename)
        {
            if (xmlReport != null)
            {
                var fn = Path.Combine(config.XMLOutputFolder, filename);
                var dr = Path.GetDirectoryName(fn);

                if (!Directory.Exists(dr))
                    Directory.CreateDirectory(dr);

                xmlReport.SaveToFile(fn, Encoding.GetEncoding("iso-8859-1"));
            }
        }

        public void ExportAllXML()
        {
            try
            {
                if (!reportCache.Any())
                    LoadCache();
            }
            catch (Exception ex)
            {
                logger.Warn("Failed to load cache. Downloading data from scratch.", ex);
                reportCache.Clear();
                progressCache.Clear();
            }

            foreach (var itm in reportCache.Values)
                SaveConvertedXML(itm);

            SaveAggregatedXML_ST();

        }

        protected void RebuildXMLCache()
        {
            //Clear out everything just in case
            XMLCache.Clear();

            //Loop over all reports up for agregation
            foreach (var report in reportCache.Where(x => x.Value.rapportId.nivaa == "stemmekrets" || x.Value.rapportId.nivaa == "fylke" || x.Value.rapportId.nivaa == "bydel").
                Select(y => y.Value))
            {
                //First we switch on election type
                switch (config.ValgType)
                {
                    case ValgTyper.ST:

                        //Then switch on level
                        switch (report.rapportId.nivaa)
                        {
                            case "fylke":
                                var id = string.Format("ST04-{0}", report.rapportId.nr);
                                XMLCache[id] = Helpers.XmlHelpersST.GetST04(report, reportCache).firstRapport;
                                break;
                            case "bydel":
                                id = string.Format("ST05-{0}", report.rapportId.nr.PadLeft(2, '0'));
                                XMLCache[id] = Helpers.XmlHelpersST.GetST05(report, reportCache).firstRapport;
                                break;
                            case "stemmekrets":
                                id = string.Format("ST03-{0}-{1}",
                                     (reportCache[report.lenker.up.href].rapportId.nivaa == "bydel" ? reportCache[report.lenker.up.href].lenker.up.nr : report.lenker.up.nr),
                                    report.rapportId.nr);
                                XMLCache[id] = Helpers.XmlHelpersST.GetST03(report, reportCache).firstRapport;
                                break;
                            default:
                                break;
                        }
                        break;

                    case ValgTyper.F:

                        //Then switch on level
                        switch (report.rapportId.nivaa)
                        {
                            case "fylke":
                                var id = string.Format("F01-{0}", report.rapportId.nr);
                                XMLCache[id] = Helpers.XmlHelpersFY.GetF01(report, reportCache).firstRapport;
                                id = string.Format("F04-{0}", report.rapportId.nr);
                                XMLCache[id] = Helpers.XmlHelpersFY.GetF04(report, reportCache).firstRapport;
                                break;
                            case "bydel":
                                //Not applicable for FY valg, Oslo is not part of FY at all, results are just copied down to Kommune-level
                                break;
                            case "stemmekrets":
                                id = string.Format("F03-{0}-{1}",
                                     (reportCache[report.lenker.up.href].rapportId.nivaa == "bydel" ? reportCache[report.lenker.up.href].lenker.up.nr : report.lenker.up.nr),
                                    report.rapportId.nr);
                                XMLCache[id] = Helpers.XmlHelpersFY.GetF03(report, reportCache).firstRapport;
                                break;
                            default:
                                break;
                        }
                        break;

                    case ValgTyper.K:

                        //Then switch on level
                        switch (report.rapportId.nivaa)
                        {
                            case "fylke":
                                var id = string.Format("K01-{0}", report.rapportId.nr);
                                XMLCache[id] = Helpers.XmlHelpersKO.GetK01(report, reportCache).firstRapport;

                                id = string.Format("K04-{0}", report.rapportId.nr);
                                XMLCache[id] = Helpers.XmlHelpersKO.GetK04(report, reportCache).firstRapport;
                                break;
                            case "bydel":
                                id = string.Format("K08-{0}", report.rapportId.nr.PadLeft(2, '0'));
                                XMLCache[id] = Helpers.XmlHelpersKO.GetK08(report, reportCache).firstRapport;
                                break;
                            case "stemmekrets":
                                id = string.Format("K03-{0}-{1}",
                                     (reportCache[report.lenker.up.href].rapportId.nivaa == "bydel" ? reportCache[report.lenker.up.href].lenker.up.nr : report.lenker.up.nr),
                                    report.rapportId.nr);
                                XMLCache[id] = Helpers.XmlHelpersKO.GetK03(report, reportCache).firstRapport;
                                break;
                            default:
                                break;
                        }
                        break;

                    case ValgTyper.SA:
                    default:
                        throw new NotImplementedException("XML conversion not implemented for this election type: " + config.ValgType);
                }
            }
            logger.InfoFormat("XML rapport cache rebuilt, {0} items", XMLCache.Count);
        }

        protected void SaveCache()
        {
            BinaryWriter writer = new BinaryWriter(new FileStream(Path.Combine(config.DataFolder, config.Name + "_cache.bin"), FileMode.Create, FileAccess.Write));
            try
            {
                writer.Write(reportCache.Count);
                foreach (var kvp in reportCache)
                {
                    writer.Write(kvp.Key);
                    writer.Write(JsonConvert.SerializeObject(kvp.Value));
                }
                logger.DebugFormat("Report CACHE containing {0} items saved to disk", reportCache.Count);

                writer.Write(progressCache.Count);
                foreach (var kvp in progressCache)
                {
                    writer.Write(kvp.Key);
                    writer.Write(kvp.Value.ToBinary());
                }
                logger.DebugFormat("Progress CACHE containing {0} items saved to disk", progressCache.Count);
            }
            finally
            {
                writer.Flush();
                writer.Close();
            }
        }

        protected void LoadCache()
        {
            BinaryReader reader = new BinaryReader(new FileStream(Path.Combine(config.DataFolder, config.Name + "_cache.bin"), FileMode.Open, FileAccess.Read));
            try
            {
                int count = reader.ReadInt32();
                reportCache = new Dictionary<string, Resultatrapport>(count);
                for (int n = 0; n < count; n++)
                {
                    var key = reader.ReadString();
                    var value = JsonConvert.DeserializeObject<Resultatrapport>(reader.ReadString());
                    reportCache.Add(key, value);
                }
                logger.InfoFormat("Report CACHE loaded with {0} items. Last JSON-root timestamp: {1}", reportCache.Count,
                    reportCache.ContainsKey(config.ReportRoot) ? reportCache[config.ReportRoot].tidspunkt.rapportGenerert : DateTime.MinValue);

                count = reader.ReadInt32();
                progressCache = new Dictionary<string, DateTime>(count);
                for (int n = 0; n < count; n++)
                {
                    var key = reader.ReadString();
                    var value = DateTime.FromBinary(reader.ReadInt64());
                    progressCache.Add(key, value);
                }
                logger.InfoFormat("Progress CACHE loaded with {0} items. Last JSON-root timestamp: {1}", progressCache.Count,
                    progressCache.ContainsKey(config.ReportRoot) ? progressCache[config.ReportRoot] : DateTime.MinValue);
            }
            finally
            {
                reader.Close();
            }
        }

    }
}
