﻿using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ValgResults.Configuration;

namespace ValgResults
{
    public partial class DebugForm : Form
    {
        /// <summary>
        /// Logger
        /// </summary>
        static ILog logger = LogManager.GetLogger(typeof(DebugForm));

        /// <summary>
        /// The actual main work object
        /// </summary>
        protected Dictionary<string, DownloadComponent> workers = new Dictionary<string, DownloadComponent>();

        public DebugForm()
        {
            log4net.Config.XmlConfigurator.Configure();
            InitializeComponent();
        }

        private void DebugForm_Load(object sender, EventArgs e)
        {
        }

        private void DebugForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                logger.Info("ValgResultsService DEBUGMODE stopping...");
                workers.Values.ToList().ForEach(w => w.Stop());
                logger.Info("ValgResultsService DEBUGMODE stopped");
            }
            catch (Exception ex)
            {
                logger.Error("ValgResultsService DEBUGMODE DID NOT STOP properly", ex);
            }

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
                logger.Info("ValgResultsService DEBUGMODE starting...");

                GlobalConfig.LoadConfig();
                ComponentConfigSection configSection =
                ConfigurationManager.GetSection("ValgComponentConfigSection") as ComponentConfigSection;

                foreach (ValgComponentConfiguration item in configSection.ValgDownloaders)
                {
                    if (item.Enabled)
                    {
                        var w = new DownloadComponent();
                        w.Configure(item);
                        workers.Add(item.Name, w);
                    }
                }

                workers.Values.ToList().ForEach(w => w.Start());
                logger.Info("ValgResultsService DEBUGMODE started");
            }
            catch (Exception ex)
            {
                logger.Error("ValgResultsService DEBUGMODE DID NOT START properly", ex);
                //workers.queryObject.Stop();
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            try
            {
                logger.Info("ValgResultsService DEBUGMODE stopping...");
                workers.Values.ToList().ForEach(w => w.Stop());
                logger.Info("ValgResultsService DEBUGMODE stopped");
            }
            catch (Exception ex)
            {
                logger.Error("ValgResultsService DEBUGMODE DID NOT STOP properly", ex);
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                logger.Info("Complete XML export started...");
                workers.Values.ToList().ForEach(w => w.ExportAllXML());
                logger.Info("Complete XML export success");
            }
            catch (Exception ex)
            {
                logger.Error("Complete XML export failed", ex);
            }

        }
    }
}
