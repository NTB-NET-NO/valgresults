﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ValgResults.Configuration
{
    class GlobalConfig
    {
        /// <summary>
        /// Log object
        /// </summary>
        static ILog logger = LogManager.GetLogger(typeof(GlobalConfig));

        static GlobalConfig()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        public static void LoadConfig()
        {
            BaseURL = ConfigurationManager.AppSettings["BaseURL"];
            logger.InfoFormat("BaseURL: {0}", BaseURL);
            UseCertificate = Convert.ToBoolean(ConfigurationManager.AppSettings["UseCertificate"]);
            logger.InfoFormat("UseCertificate: {0}", UseCertificate);
            CertificatePath = ConfigurationManager.AppSettings["CertificatePath"];
            logger.InfoFormat("CertificatePath: {0}", CertificatePath);

            CreateXMLProgress = Convert.ToBoolean(ConfigurationManager.AppSettings["GenereateXMLProgress"]);
            logger.DebugFormat("CreateXMLProgress: {0}", CreateXMLProgress);
            CreateJSONProgress = Convert.ToBoolean(ConfigurationManager.AppSettings["GenereateJSONProgress"]);
            logger.DebugFormat("CreateJSONProgress: {0}", CreateJSONProgress);
            CreateJSONAggregates = Convert.ToBoolean(ConfigurationManager.AppSettings["GenereateJSONAggregates"]);
            logger.DebugFormat("CreateJSONAggregates: {0}", CreateJSONAggregates);
            JSONProgressDataCap = Convert.ToInt32(ConfigurationManager.AppSettings["JSONProgressDataElementCap"]);
            logger.DebugFormat("JSONProgressDataCap: {0}", JSONProgressDataCap);

            try
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["CertificatePath"]))
                    Certificate = X509Certificate.CreateFromCertFile(ConfigurationManager.AppSettings["CertificatePath"]);
                logger.InfoFormat("Loaded Certificate: {0}/{1}", Certificate.Subject, Certificate.Issuer);
            }
            catch (Exception ex)
            {
                logger.WarnFormat("Error loading certificate {0}, running without. {1}", ConfigurationManager.AppSettings["certificatePath"], ex.Message);
            }
        }

        public static string BaseURL { get; private set; }
        public static bool UseCertificate { get; private set; }
        public static string CertificatePath { get; private set; }
        public static X509Certificate Certificate { get; private set; }

        public static bool CreateXMLProgress { get; private set; }
        public static bool CreateJSONProgress { get; private set; }
        public static bool CreateJSONAggregates { get; private set; }
        public static int JSONProgressDataCap { get; private set; }

    }
}
