﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValgResults.Configuration
{
    //General valg type enum
    public enum ValgTyper { ST, F, K, SA };

    //Config section for one component
    public class ValgComponentConfiguration : ConfigurationElement
    {
        public ValgComponentConfiguration() { }

        public ValgComponentConfiguration(string name, ValgTyper valgType, bool enabled, int pollInterval, string reportRoot, 
            string dataFolder, string jsonOutputFolder, string xmlOutputFolder)
        {
            Name = name;
            ValgType = valgType;
            Enabled = enabled;
            PollInterval = pollInterval;
            ReportRoot = reportRoot;
            DataFolder = dataFolder;
            JSONOutputFolder = jsonOutputFolder;
            XMLOutputFolder = xmlOutputFolder;
        }

        [ConfigurationProperty("Name", IsRequired = true, IsKey = true)]
        public string Name
        {
            get { return (string)this["Name"]; }
            set { this["Name"] = value; }
        }

        [ConfigurationProperty("ValgType", IsRequired = true, IsKey = false)]
        public ValgTyper ValgType
        {
            get { return (ValgTyper)this["ValgType"]; }
            set { this["ValgType"] = value; }
        }

        [ConfigurationProperty("Enabled", IsRequired = false, DefaultValue = true, IsKey = false)]
        public bool Enabled
        {
            get { return (bool)this["Enabled"]; }
            set { this["Enabled"] = value; }
        }

        [ConfigurationProperty("PollInterval", IsRequired = false, DefaultValue = 60, IsKey = false)]
        public int PollInterval
        {
            get { return (int)this["PollInterval"]; }
            set { this["PollInterval"] = value; }
        }

        [ConfigurationProperty("ReportRoot", IsRequired = true, IsKey = false)]
        public string ReportRoot
        {
            get { return (string)this["ReportRoot"]; }
            set { this["ReportRoot"] = value; }
        }

        [ConfigurationProperty("DataFolder", IsRequired = true, IsKey = false)]
        public string DataFolder
        {
            get { return (string)this["DataFolder"]; }
            set { this["DataFolder"] = value; }
        }

        [ConfigurationProperty("JSONOutputFolder", IsRequired = false, IsKey = false)]
        public string JSONOutputFolder
        {
            get { return string.IsNullOrWhiteSpace((string)this["JSONOutputFolder"]) ? Path.Combine(DataFolder, "json") : (string)this["JSONOutputFolder"]; }
            set { this["JSONOutputFolder"] = value; }
        }

        [ConfigurationProperty("XMLOutputFolder", IsRequired = false, IsKey = false)]
        public string XMLOutputFolder
        {
            get { return string.IsNullOrWhiteSpace((string)this["XMLOutputFolder"]) ? Path.Combine(DataFolder, "xml") : (string)this["XMLOutputFolder"]; }
            set { this["XMLOutputFolder"] = value; }
        }

        [ConfigurationProperty("XMLReportTypeFolders", IsRequired = false, DefaultValue = true, IsKey = false)]
        public bool XMLReportTypeFolders
        {
            get { return (bool)this["XMLReportTypeFolders"]; }
            set { this["XMLReportTypeFolders"] = value; }
        }

        [ConfigurationProperty("DisableXMLReports", IsRequired = false, DefaultValue = false, IsKey = false)]
        public bool DisableXMLReports
        {
            get { return (bool)this["DisableXMLReports"]; }
            set { this["DisableXMLReports"] = value; }
        }
    }


    //List of configs
    public class ComponentConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("ValgDownloaders", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(ValgComponentConfigCollection),
            AddItemName = "DownloadComponent")]
        public ValgComponentConfigCollection ValgDownloaders
        {
            get
            {
                return (ValgComponentConfigCollection)base["ValgDownloaders"];
            }
        }
    }

    //Config collection helper
    public class ValgComponentConfigCollection : ConfigurationElementCollection
    {
        public ValgComponentConfiguration this[int index]
        {
            get { return (ValgComponentConfiguration)BaseGet(index); }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        public void Add(ValgComponentConfiguration serviceConfig)
        {
            BaseAdd(serviceConfig);
        }

        public void Clear()
        {
            BaseClear();
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ValgComponentConfiguration();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ValgComponentConfiguration)element).Name;
        }

        public void Remove(ValgComponentConfiguration serviceConfig)
        {
            BaseRemove(serviceConfig.Name);
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }
    }
}
