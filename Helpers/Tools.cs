﻿using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ValgResults.Configuration;
using ValgResults.DataObjects;

namespace ValgResults.Helpers
{
    class Tools
    {
        /// <summary>
        /// Log object
        /// </summary>
        static ILog logger = LogManager.GetLogger(typeof(Tools));

        static Tools()
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            log4net.Config.XmlConfigurator.Configure();
        }

        /// <summary>
        /// Sends the query to the server and returns the fetched XML.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns></returns>
        public static string DoQuery(string query)
        {
            string ret = "";

            //Create the URL
            string url = GlobalConfig.BaseURL + query;
            logger.Debug("DoQuery request: " + url);

            //Create the HTTP request
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.Timeout = 1000; // QueryComponent.timeout;

            //Load Cert
            if (GlobalConfig.UseCertificate)
            {
                request.ClientCertificates.Add(GlobalConfig.Certificate);
            }
            
            //Get the HTTP response with the resulting data
            HttpWebResponse response;
            try
            {
                response = (HttpWebResponse)request.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                ret = reader.ReadToEnd();
                reader.Close();
                logger.Debug("DoQuery request completed OK");
            }
            catch (WebException webEx)
            {
                logger.Error("HTTP request failed: " + webEx.Message);
                if (webEx.Response != null) webEx.Response.Close();
            }
            catch (Exception ex)
            {
                logger.Error("General request error: " + ex.Message, ex);
            }

            return ret;
        }

        /// <summary>
        /// Dumps xml content to file.
        /// </summary>
        /// <param name="XML">The XML.</param>
        /// <param name="path">The path.</param>
        /// <param name="filename">The filename.</param>
        public static void DumpToFile(string content, string path, string filename)
        {
            //Handle directory structure
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                logger.DebugFormat("Directory created: {0}", path);
            }

            //Dump the data
            StreamWriter wrt = new StreamWriter(Path.Combine(path, filename), false, Encoding.UTF8);
            wrt.WriteLine(content);
            wrt.Close();
            wrt = null;
            logger.Debug("Saved " + Path.Combine(path, filename) + " to disk.");
        }

        public static string FormatDecimal(double? val, bool forceSign = false, string precision = "F1")
        {
            var retVal = val.HasValue ? val.Value.ToString(precision, CultureInfo.InvariantCulture) : null as string;
            return (forceSign ? AddSign(retVal) : retVal);
        }

        public static string AddSign(string formattedVal)
        {
            return (!string.IsNullOrEmpty(formattedVal) && !formattedVal.StartsWith("-") ? "+" + formattedVal : formattedVal);
        }

        public static string AddSign(int val)
        {
            return AddSign(Convert.ToString(val));
        }

        public static string AddSign(object val)
        {
            if (val != null)
                return AddSign(Convert.ToString(val));
            else
                return "null";
        }


        //<!--
        // Statusindikator for innrapportering har følgende verdier
        // 0.	Ingen resultater innsendt
        // 1	Bare foreløpige forhåndsstemmer innsendt
        // 2.	Endelige fhst. innsendt
        // 3.	Bare foreløpige valgtingsstemmer innsendt
        // 4.	Endelige vtst. innsendt
        // 5.	Foreløpige fhst. og foreløpige vtst. innsendt
        // 6.	Endelige fhst. og foreløpige vtst. innsendt
        // 7.	Foreløpige fhst. og endelige vtst. innsendt
        // 8.	Endelige fhst. og endelige vtst. Innsendt
        // -->

        // "stemmer"   : { "total": 2112, "fhs": 199, "totalForkastede": 0, "fhsForkastede": 0 },
        // "opptalt"   : { "prosent": 100.0, "foreløpigFhs": 100.0, "foreløpigVts": 100.0, "foreløpig": 100.0, "endelig": 100.0, "oppgjør": 100.0 },        
        public static int GetStatusIndicator(Resultatrapport report)
        {
            //Any votes at all?
            if (report.stemmesedler.total > 0)
            {
                // 8 - endelig = 100.0 is always 8, will also handle cases with kretser without FHS/VTS stopping at 2/4.
                if (report.opptalt.endelig >= 100)
                    return 8;

                // 1 or 2
                if (report.stemmesedler.total == report.stemmesedler.fhs)
                    return (report.opptalt.endelig > 0 ? 2 : 1);

                // 3 or 4
                if (report.stemmesedler.fhs == 0)
                    return (report.opptalt.endelig > 0 ? 4 : 3);

                // 5 or 6/7
                return (report.opptalt.endelig > 0 ? 6 : 5);
                // Unable to tell the differences between 6 or 7 based on available data, assuming 6 with partly endelig results
            }

            //else return zero
            return 0;
        }

        public static string BuildFilename(Resultatrapport report)
        {
            //Return a unique string for filenames
            return report.lenker.self.href.Replace("/", "-").TrimStart('-').ToUpper();

        }

        public static string BuildIdString(Resultatrapport report)
        {
            //Return a unique string for logging and ref
            return string.Format("{0}-{1}-{2}",
                report.lenker.self.href.Replace("/", "-").TrimStart('-').ToUpper(),
                report.rapportId.nivaa,
                report.rapportId.navn.Replace("/", "-"));

        }

    }
}
