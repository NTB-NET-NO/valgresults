﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ValgResults.DataObjects;

namespace ValgResults.Helpers
{
    class XmlHelpers
    {
        public static respons GetNewXMLReport(string title, DateTime timestamp)
        {
            var blankDoc = new respons();
            var r = new responsRapport();

            r.rapportnavn = title;
            r.data.Add(
                 new responsRapportData() { navn = "SisteRegDato", Value = timestamp.ToString("yyyyMMdd", CultureInfo.InvariantCulture) }
                );
            r.data.Add(
                new responsRapportData() { navn = "SisteRegTid", Value = timestamp.ToString("HH:mm:ss", CultureInfo.InvariantCulture) }
               );

            blankDoc.rapport.Add(r);
            return blankDoc;
        }

        //Custom progress report
        public static respons GetP99(Dictionary<string, Resultatrapport> reportCache, string electionType)
        {
            //Start with blank new XML-report
            var doc = XmlHelpers.GetNewXMLReport(electionType + "99", DateTime.Now);
            doc.rapport.Clear();

            //Get some data from the cache
            var fylker = reportCache.Values.Where(r => r.rapportId.nivaa == "fylke").OrderBy(x => x.rapportId.nr);
            var kommuner = reportCache.Values.Where(r => r.rapportId.nivaa == "kommune").OrderBy(x => x.rapportId.nr);

            //Progress data, per fylke
            foreach (var f in fylker)
            {
                var tmpDoc = XmlHelpers.GetNewXMLReport(electionType + "99", DateTime.Now);
                tmpDoc.firstRapport.data.Add(new responsRapportData() { navn="FylkeNr", Value = f.rapportId.nr});
                tmpDoc.firstRapport.data.Add(new responsRapportData() { navn = "FylkeNavn", Value = f.rapportId.navn });
                
                //Progress data per kommune
                var tabell = new responsRapportTabell() { navn = electionType + "99tabell1" };
                foreach (var k in kommuner.Where(x => x.lenker.up.nr == f.rapportId.nr))
                {
                    var statusInd = k.StatusIndikator;

                    var subs = reportCache.Where(r => r.Value.rapportId.nivaa == "stemmekrets" && r.Key.StartsWith(k.lenker.self.href));
                    if (subs.Where(x => x.Value.StatusIndikator > 0).Any())
                        statusInd = subs.Where(x => x.Value.StatusIndikator > 0).Min(y => y.Value.StatusIndikator);


                    //Print out all progress data ordered by KommNr
                    var l = new responsRapportTabellListe();
                    l.data.Add(new responsRapportTabellListeData() { navn = "KommNr", Value = k.rapportId.nr });
                    l.data.Add(new responsRapportTabellListeData() { navn = "KommNavn", Value = k.rapportId.navn });
                    l.data.Add(new responsRapportTabellListeData() { navn = "FylkeNr", Value = k.lenker.up.nr });
                    l.data.Add(new responsRapportTabellListeData() { navn = "FylkeNavn", Value = k.lenker.up.navn });
                    l.data.Add(new responsRapportTabellListeData() { navn = "StatusInd", Value = Convert.ToString(statusInd) });
                    l.data.Add(new responsRapportTabellListeData() { navn = "SisteRegDato", Value = k.tidspunkt.rapportGenerert.ToString("yyyyMMdd", CultureInfo.InvariantCulture) });
                    l.data.Add(new responsRapportTabellListeData() { navn = "SisteRegTid", Value = k.tidspunkt.rapportGenerert.ToString("HH:mm:ss", CultureInfo.InvariantCulture) });

                    tabell.liste.Add(l);
                }
                tmpDoc.firstRapport.tabell.Add(tabell);
                doc.rapport.Add(tmpDoc.firstRapport);
            }
            return doc;
        }
    }

    class XmlHelpersKO
    {
        //Fylke - progress
        public static respons GetK01(Resultatrapport report, Dictionary<string, Resultatrapport> reportCache)
        {
            //Start with blank new XML-report
            var doc = XmlHelpers.GetNewXMLReport("K01", report.tidspunkt.rapportGenerert);

            //Not using calculated StatusInd here, but same logic is applied
            var totKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr));
            var fhsKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr) &&
                Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == Resultatrapport.stemmesedler.total);
            var vtsKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr) &&
                Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == 0);
            var altKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr) &&
                Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs != Resultatrapport.stemmesedler.total);
            var alt8Kom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr) && Resultatrapport.opptalt.endelig == 100);

            //Use StatusInd for krets count logic
            //Apply krets count to relevant selection only, extra selector needed as krets-numbers does not include fylke/kommune numbers. Use StatusInd for logic
            var kretser = reportCache.Where(Resultatrapport => Resultatrapport.Value.rapportId.nivaa == "stemmekrets" && Resultatrapport.Key.StartsWith(report.lenker.self.href)).Select(rr => rr.Value);
            var totKrets = kretser.Count();
            var fhsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 1 || Resultatrapport.StatusIndikator == 2);
            var vtsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 3 || Resultatrapport.StatusIndikator == 4);
            var altKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator >= 5);
            var alt8Krets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 8);

            //Actuall XML generation
            doc.firstRapport.data.Add(new responsRapportData() { navn = "FylkeNr", Value = report.rapportId.nr });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "FylkeNavn", Value = report.rapportId.navn });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKomm", Value = Convert.ToString(totKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommFhstOpptalt", Value = Convert.ToString(fhsKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommVtstOpptalt", Value = Convert.ToString(vtsKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommAltOpptalt", Value = Convert.ToString(altKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommAltOppt8", Value = Convert.ToString(alt8Kom) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKretser", Value = Convert.ToString(totKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserFhstOpptalt", Value = Convert.ToString(fhsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserVtstOpptalt", Value = Convert.ToString(vtsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOpptalt", Value = Convert.ToString(altKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOppt8", Value = Convert.ToString(alt8Krets) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProgProStOpptalt", Value = Tools.FormatDecimal(report.opptalt.prosent) });

            //Progress data
            var tabell = new responsRapportTabell() { navn = "K01tabell1" };
            foreach (var k in reportCache.Where(x => x.Key.StartsWith(report.lenker.self.href) && !x.Value.lenker.subrapporter.Any() && x.Value.StatusIndikator > 0 &&
                (x.Value.rapportId.nivaa == "kommune" || x.Value.rapportId.nivaa == "stemmekrets")))
            {
                //This is a change from earlier elections, stemmekrets was not orignally present fro K01/F01.
                //This is handeled in the XSLT memnu logic on the web page
                var l = new responsRapportTabellListe();
                if (k.Value.rapportId.nivaa == "stemmekrets")
                {
                    l.data.Add(new responsRapportTabellListeData()
                    {
                        navn = "KommNr",
                        Value =
                            (reportCache[k.Value.lenker.up.href].rapportId.nivaa == "bydel" ? reportCache[k.Value.lenker.up.href].lenker.up.nr : k.Value.lenker.up.nr)
                    });
                    l.data.Add(new responsRapportTabellListeData()
                    {
                        navn = "KommNavn",
                        Value =
                            (reportCache[k.Value.lenker.up.href].rapportId.nivaa == "bydel" ? reportCache[k.Value.lenker.up.href].lenker.up.navn : k.Value.lenker.up.navn)
                    });
                    l.data.Add(new responsRapportTabellListeData() { navn = "KretsNr", Value = k.Value.rapportId.nr });
                    l.data.Add(new responsRapportTabellListeData() { navn = "KretsNavn", Value = k.Value.rapportId.navn });

                }
                else
                {
                    l.data.Add(new responsRapportTabellListeData() { navn = "KommNr", Value = k.Value.rapportId.nr });
                    l.data.Add(new responsRapportTabellListeData() { navn = "KommNavn", Value = k.Value.rapportId.navn });
                    l.data.Add(new responsRapportTabellListeData() { navn = "KretsNr" });
                    l.data.Add(new responsRapportTabellListeData() { navn = "KretsNavn" });
                }
                l.data.Add(new responsRapportTabellListeData() { navn = "StatusInd", Value = Convert.ToString(k.Value.StatusIndikator) });
                tabell.liste.Add(l);
            }

            doc.firstRapport.tabell.Add(tabell);
            return doc;
        }

        //Kommune
        public static respons GetK02(Resultatrapport report, Dictionary<string, Resultatrapport> reportCache)
        {
            //Start with blank new XML-report
            var doc = XmlHelpers.GetNewXMLReport("K02", report.tidspunkt.rapportGenerert);

            //Apply krets count to relevant selection only, extra selector needed as krets-numbers does not include fylke/kommune numbers. Use StatusInd for logic.
            var kretser = reportCache.Where(Resultatrapport => Resultatrapport.Value.rapportId.nivaa == "stemmekrets" &&
                Resultatrapport.Key.StartsWith(report.lenker.self.href)).Select(rr => rr.Value);
            var totKrets = kretser.Count();
            var fhsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 1 || Resultatrapport.StatusIndikator == 2);
            var vtsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 3 || Resultatrapport.StatusIndikator == 4);
            var altKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator >= 5);
            var alt8Krets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 8);

            //Actuall XML generation
            doc.firstRapport.data.Add(new responsRapportData() { navn = "FylkeNr", Value = report.lenker.up.nr });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "FylkeNavn", Value = report.lenker.up.navn });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "KommNr", Value = report.rapportId.nr });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "KommNavn", Value = report.rapportId.navn });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKretser", Value = Convert.ToString(totKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserFhstOpptalt", Value = Convert.ToString(fhsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserVtstOpptalt", Value = Convert.ToString(vtsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOpptalt", Value = Convert.ToString(altKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOppt8", Value = Convert.ToString(alt8Krets) });

            //Set StatusInd as applicable
            if (!report.lenker.subrapporter.Any())
                doc.firstRapport.data.Add(new responsRapportData() { navn = "StatusInd", Value = Convert.ToString(report.StatusIndikator) });
            else
                doc.firstRapport.data.Add(new responsRapportData() { navn = "StatusInd" });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProgProStOpptalt", Value = Tools.FormatDecimal(report.opptalt.prosent) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntStBerett", Value = Convert.ToString(report.antallsb) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFrammotte", Value = Convert.ToString(report.stemmesedler.total) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProFrammotte", Value = Tools.FormatDecimal(Math.Max(report.frammote.prosent.GetValueOrDefault(), report.frammote.prosentStemmegiving.GetValueOrDefault())) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "DiffFrammFKsv", Value = "NA" });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "DiffPropFrammFKsv", Value = Tools.FormatDecimal(report.frammote.endring) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "DiffFrammFStv", Value = "NA" });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "DiffPropFrammFStv", Value = "NA" });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFhstOpptalt", Value = Convert.ToString(report.stemmesedler.fhs) });
            //doc.firstRapport.data.Add(new responsRapportData() { navn = "EvalgInkl", Value = "NA" });

            //Looping parti-liste data
            var tabell = new responsRapportTabell() { navn = "K02tabell1" };
            foreach (var item in report.partier)
            {
                var liste = new responsRapportTabellListe();
                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikode", Value = item.partiNavn.partikode });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntFhst", Value = Convert.ToString(item.stemmetall.resultat.antall.fhs) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntStemmer", Value = Convert.ToString(item.stemmetall.resultat.antall.total) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProSt", Value = Tools.FormatDecimal(item.stemmetall.resultat.prosent) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffStFKsv", Value = "NA" });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFKsv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.samme, true) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffStFStv", Value = "NA" });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFStv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.ekvivalent, true) });

                //Prognosetall if avail
                //Extra check on mandatattall needed to handle certain cases
                //if (report.prognose.beregnet && item.mandattall != null)
                //{
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgProSt", Value = Tools.FormatDecimal(item.stemmetall.prognose.prosent) });
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFKsv", Value = Tools.FormatDecimal(item.stemmetall.prognose.endring.samme, true) });
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFStv", Value = Tools.FormatDecimal(item.stemmetall.prognose.endring.ekvivalent, true) });

                //    //Fylkesmandater
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndt", Value = Convert.ToString(item.mandattall.prognose.antall) });
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndt", Value = Tools.AddSign(item.mandattall.prognose.endring) });
                //}
                //else
                if (item.mandattall != null)
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgProSt", Value = "-" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFKsv", Value = "-" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFStv", Value = "-" });

                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndt", Value = Convert.ToString(item.mandattall.resultat.antall) });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndt", Value = Tools.AddSign(item.mandattall.resultat.endring) });
                }
                else
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgProSt", Value = "-" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFKsv", Value = "-" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFStv", Value = "-" });

                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndt" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndt" });
                }


                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikategori", Value = Convert.ToString(item.partiNavn.partikategori) });
                liste.tabell = null;
                tabell.liste.Add(liste);
            }

            doc.firstRapport.tabell.Add(tabell);
            return doc;
        }

        //Krets
        public static respons GetK03(Resultatrapport report, Dictionary<string, Resultatrapport> reportCache)
        {
            //Start with blank new XML-report
            var doc = XmlHelpers.GetNewXMLReport("K03", report.tidspunkt.rapportGenerert);

            //Determine correct aggregation parent
            var parent = reportCache[report.lenker.up.href].rapportId.nivaa == "bydel" ? reportCache[report.lenker.up.href].lenker.up : report.lenker.up;

            //Apply krets count to relevant selection only, extra selector needed as krets-numbers does not include fylke/kommune numbers. Use StatusInd for logic.
            var kretser = reportCache.Where(Resultatrapport => Resultatrapport.Value.rapportId.nivaa == "stemmekrets" &&
                Resultatrapport.Key.StartsWith(parent.href)).Select(rr => rr.Value);
            var totKrets = kretser.Count();
            var fhsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 1 || Resultatrapport.StatusIndikator == 2);
            var vtsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 3 || Resultatrapport.StatusIndikator == 4);
            var altKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator >= 5);
            var alt8Krets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 8);

            //Actuall XML generation
            doc.firstRapport.data.Add(new responsRapportData() { navn = "KommNr", Value = parent.nr });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "KommNavn", Value = parent.navn });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKretser", Value = Convert.ToString(totKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserFhstOpptalt", Value = Convert.ToString(fhsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserVtstOpptalt", Value = Convert.ToString(vtsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOpptalt", Value = Convert.ToString(altKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOppt8", Value = Convert.ToString(alt8Krets) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "KretsNr", Value = report.rapportId.nr });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "KretsNavn", Value = report.rapportId.navn });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "StatusInd", Value = Convert.ToString(report.StatusIndikator) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntStBerett", Value = Convert.ToString(report.antallsb) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFrammotte", Value = Convert.ToString(report.stemmesedler.total) });

            //Use Stemmegiving for kretser, will give a reasonable value when frammøte is missing
            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProFrammotte", Value = Tools.FormatDecimal(Math.Max(report.frammote.prosent.GetValueOrDefault(), report.frammote.prosentStemmegiving.GetValueOrDefault())) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFhstOpptalt", Value = Convert.ToString(report.stemmesedler.fhs) });

            //Looping parti-liste data
            var tabell = new responsRapportTabell() { navn = "K03tabell1" };
            foreach (var item in report.partier)
            {
                var liste = new responsRapportTabellListe();
                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikode", Value = item.partiNavn.partikode });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntFhst", Value = Convert.ToString(item.stemmetall.resultat.antall.fhs) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntStemmer", Value = Convert.ToString(item.stemmetall.resultat.antall.total) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProSt", Value = Tools.FormatDecimal(item.stemmetall.resultat.prosent) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFKsv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.samme, true) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFStv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.ekvivalent, true) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikategori", Value = Convert.ToString(item.partiNavn.partikategori) });
                liste.tabell = null;
                tabell.liste.Add(liste);
            }

            doc.firstRapport.tabell.Add(tabell);
            return doc;
        }

        //Fylke
        public static respons GetK04(Resultatrapport report, Dictionary<string, Resultatrapport> reportCache)
        {
            //Start with blank new XML-report
            var doc = XmlHelpers.GetNewXMLReport("K04", report.tidspunkt.rapportGenerert);

            //Misc calculations

            //Not using calculated StatusInd here, but same logic is applied
            var totKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr));
            var fhsKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr) &&
                Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == Resultatrapport.stemmesedler.total);
            var vtsKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr) &&
                Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == 0);
            var altKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr) &&
                Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs != Resultatrapport.stemmesedler.total);
            var alt8Kom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr) &&
                Resultatrapport.opptalt.endelig == 100);

            //Apply krets count to relevant selection only, extra selector needed as krets-numbers does not include fylke/kommune numbers. Use StatusInd for logic
            var kretser = reportCache.Where(Resultatrapport => Resultatrapport.Value.rapportId.nivaa == "stemmekrets" && Resultatrapport.Key.StartsWith(report.lenker.self.href)).Select(rr => rr.Value);
            var totKrets = kretser.Count();
            var fhsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 1 || Resultatrapport.StatusIndikator == 2);
            var vtsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 3 || Resultatrapport.StatusIndikator == 4);
            var altKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator >= 5);
            var alt8Krets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 8);

            //Actuall XML generation
            doc.firstRapport.data.Add(new responsRapportData() { navn = "FylkeNr", Value = report.rapportId.nr });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "FylkeNavn", Value = report.rapportId.navn });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKomm", Value = Convert.ToString(totKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommFhstOpptalt", Value = Convert.ToString(fhsKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommVtstOpptalt", Value = Convert.ToString(vtsKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommAltOpptalt", Value = Convert.ToString(altKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommAltOppt8", Value = Convert.ToString(alt8Kom) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKretser", Value = Convert.ToString(totKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserFhstOpptalt", Value = Convert.ToString(fhsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserVtstOpptalt", Value = Convert.ToString(vtsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOpptalt", Value = Convert.ToString(altKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOppt8", Value = Convert.ToString(alt8Krets) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProgProStOpptalt", Value = Tools.FormatDecimal(report.opptalt.prosent) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntStBerett", Value = Convert.ToString(report.antallsb) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFrammotte", Value = Convert.ToString(report.stemmesedler.total) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProFrammotte", Value = Tools.FormatDecimal(Math.Max(report.frammote.prosent.GetValueOrDefault(), report.frammote.prosentStemmegiving.GetValueOrDefault())) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFhstOpptalt", Value = Convert.ToString(report.stemmesedler.fhs) });

            //Looping parti-liste data
            var tabell = new responsRapportTabell() { navn = "K04tabell1" };
            foreach (var item in report.partier)
            {
                var liste = new responsRapportTabellListe();
                //Opptalt data
                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikode", Value = item.partiNavn.partikode });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntFhst", Value = Convert.ToString(item.stemmetall.resultat.antall.fhs) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntStemmer", Value = Convert.ToString(item.stemmetall.resultat.antall.total) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProSt", Value = Tools.FormatDecimal(item.stemmetall.resultat.prosent) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFKsv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.samme, true) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFStv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.ekvivalent, true) });

                //Prognosetall if avail
                //if (report.prognose.beregnet)
                //{
                //    //Fylkesmandater
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndt", Value = Convert.ToString(item.mandattall.prognose.antall) });
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndt", Value = Tools.AddSign(item.mandattall.prognose.endring) });
                //}

                //Extra check on mandatattall needed to handle certain cases. Appears that mandater for KO at fylkeslevel takes a while to appear
                if (item.mandattall != null)
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndt", Value = Convert.ToString(item.mandattall.resultat.antall) });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndt", Value = Tools.AddSign(item.mandattall.resultat.endring) });
                }
                else
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndt" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndt" });
                }

                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikategori", Value = Convert.ToString(item.partiNavn.partikategori) });
                liste.tabell = null;
                tabell.liste.Add(liste);
            }

            doc.firstRapport.tabell.Add(tabell);

            return doc;
        }

        //Land
        public static respons GetK05(Resultatrapport report, Dictionary<string, Resultatrapport> reportCache)
        {
            //Start with blank new XML-report
            var doc = XmlHelpers.GetNewXMLReport("K05", report.tidspunkt.rapportGenerert);

            //Misc calculations

            //Not using calculated StatusInd here, but same logic is applied
            var totKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune");
            var fhsKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" &&
                Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == Resultatrapport.stemmesedler.total);
            var vtsKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" &&
                Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == 0);
            var altKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" &&
                Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs != Resultatrapport.stemmesedler.total);
            var alt8Kom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.opptalt.endelig == 100);

            //Use StatusInd for krets count logic
            var totKrets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets");
            var fhsKrets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && (Resultatrapport.StatusIndikator == 1 || Resultatrapport.StatusIndikator == 2));
            var vtsKrets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && (Resultatrapport.StatusIndikator == 3 || Resultatrapport.StatusIndikator == 4));
            var altKrets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && Resultatrapport.StatusIndikator >= 5);
            var alt8Krets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && Resultatrapport.StatusIndikator == 8);

            ////Not using calculated StatusInd here, but same logic is applied
            //var totKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr));
            //var fhsKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr) &&
            //    Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == Resultatrapport.stemmesedler.total);
            //var vtsKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr) &&
            //    Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == 0);
            //var altKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr) &&
            //    Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs != Resultatrapport.stemmesedler.total);
            //var alt8Kom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr) &&
            //    Resultatrapport.opptalt.endelig == 100);

            ////Apply krets count to relevant selection only, extra selector needed as krets-numbers does not include fylke/kommune numbers. Use StatusInd for logic
            //var kretser = reportCache.Where(Resultatrapport => Resultatrapport.Value.rapportId.nivaa == "stemmekrets" && Resultatrapport.Key.StartsWith(report.lenker.self.href)).Select(rr => rr.Value);
            //var totKrets = kretser.Count();
            //var fhsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 1 || Resultatrapport.StatusIndikator == 2);
            //var vtsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 3 || Resultatrapport.StatusIndikator == 4);
            //var altKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator >= 5);
            //var alt8Krets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 8);

            //Actuall XML generation
            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKomm", Value = Convert.ToString(totKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommFhstOpptalt", Value = Convert.ToString(fhsKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommVtstOpptalt", Value = Convert.ToString(vtsKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommAltOpptalt", Value = Convert.ToString(altKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommAltOppt8", Value = Convert.ToString(alt8Kom) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKretser", Value = Convert.ToString(totKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserFhstOpptalt", Value = Convert.ToString(fhsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserVtstOpptalt", Value = Convert.ToString(vtsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOpptalt", Value = Convert.ToString(altKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOppt8", Value = Convert.ToString(alt8Krets) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProgProStOpptalt", Value = Tools.FormatDecimal(report.opptalt.prosent) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntStBerett", Value = Convert.ToString(report.antallsb) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFrammotte", Value = Convert.ToString(report.stemmesedler.total) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProFrammotte", Value = Tools.FormatDecimal(Math.Max(report.frammote.prosent.GetValueOrDefault(), report.frammote.prosentStemmegiving.GetValueOrDefault())) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFhstOpptalt", Value = Convert.ToString(report.stemmesedler.fhs) });

            //Looping parti-liste data
            var tabell = new responsRapportTabell() { navn = "K05tabell1" };
            foreach (var item in report.partier)
            {
                var liste = new responsRapportTabellListe();
                //Opptalt data
                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikode", Value = item.partiNavn.partikode });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntFhst", Value = Convert.ToString(item.stemmetall.resultat.antall.fhs) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntStemmer", Value = Convert.ToString(item.stemmetall.resultat.antall.total) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProSt", Value = Tools.FormatDecimal(item.stemmetall.resultat.prosent) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFKsv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.samme, true) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFStv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.ekvivalent, true) });

                //Prognosetall if avail
                //if (report.prognose.beregnet)
                //{
                //    //Fylkesmandater
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndt", Value = Convert.ToString(item.mandattall.prognose.antall) });
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndt", Value = Tools.AddSign(item.mandattall.prognose.endring) });
                //}

                //Extra check on mandatattall needed to handle certain cases. Appears that mandater for KO at national level takes a while to appear
                if (item.mandattall != null)
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndt", Value = Convert.ToString(item.mandattall.resultat.antall) });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndt", Value = Tools.AddSign(item.mandattall.resultat.endring) });
                }
                else
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndt" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndt" });
                }

                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikategori", Value = Convert.ToString(item.partiNavn.partikategori) });
                liste.tabell = null;
                tabell.liste.Add(liste);
            }

            doc.firstRapport.tabell.Add(tabell);

            return doc;
        }

        //Bydel
        public static respons GetK08(Resultatrapport report, Dictionary<string, Resultatrapport> reportCache)
        {
            //Start with blank new XML-report
            var doc = XmlHelpers.GetNewXMLReport("K08", report.tidspunkt.rapportGenerert);

            //Apply krets count to relevant selection only, extra selector needed as krets-numbers does not include fylke/kommune numbers. Use StatusInd for logic.
            var kretser = reportCache.Where(Resultatrapport => Resultatrapport.Value.rapportId.nivaa == "stemmekrets" &&
                Resultatrapport.Value.lenker.up.href == report.lenker.self.href).Select(rr => rr.Value);
            var totKrets = kretser.Count();
            var fhsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 1 || Resultatrapport.StatusIndikator == 2);
            var vtsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 3 || Resultatrapport.StatusIndikator == 4);
            var altKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator >= 5);
            var alt8Krets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 8);

            //Actuall XML generation
            doc.firstRapport.data.Add(new responsRapportData() { navn = "KommNr", Value = report.lenker.up.nr });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "KommNavn", Value = report.lenker.up.navn });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "BydelNr", Value = report.rapportId.nr.PadLeft(2,'0') });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "BydelNavn", Value = report.rapportId.navn });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKretser", Value = Convert.ToString(totKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserFhstOpptalt", Value = Convert.ToString(fhsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserVtstOpptalt", Value = Convert.ToString(vtsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOpptalt", Value = Convert.ToString(altKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOppt8", Value = Convert.ToString(alt8Krets) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntStBerett", Value = Convert.ToString(report.antallsb) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFrammotte", Value = Convert.ToString(report.stemmesedler.total) });

            //Use Stemmegiving for kretser, will give a reasonable value when frammøte is missing
            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProFrammotte", Value = Tools.FormatDecimal(Math.Max(report.frammote.prosent.GetValueOrDefault(), report.frammote.prosentStemmegiving.GetValueOrDefault())) });

            //Looping parti-liste data
            var tabell = new responsRapportTabell() { navn = "K08tabell1" };
            foreach (var item in report.partier)
            {
                var liste = new responsRapportTabellListe();
                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikode", Value = item.partiNavn.partikode });
                //liste.data.Add(new responsRapportTabellListeData() { navn = "AntFhst", Value = Convert.ToString(item.stemmetall.resultat.antall.fhs) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntStemmer", Value = Convert.ToString(item.stemmetall.resultat.antall.total) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProSt", Value = Tools.FormatDecimal(item.stemmetall.resultat.prosent) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFKsv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.samme, true) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFStv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.ekvivalent, true) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikategori", Value = Convert.ToString(item.partiNavn.partikategori) });
                liste.tabell = null;
                tabell.liste.Add(liste);
            }

            doc.firstRapport.tabell.Add(tabell);
            return doc;
        }

        //Land - Frammøte topp/bunn 10
        public static respons GetK07(Dictionary<string, Resultatrapport> reportCache, string rootRef)
        {
            //Start with blank new XML-report
            var doc = XmlHelpers.GetNewXMLReport("K07", reportCache[rootRef].tidspunkt.rapportGenerert);

            //Misc calculations
            //Not using calculated StatusInd here, but same logic is applied
            var komListe = reportCache.Values.Where(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune");
            var totKom = komListe.Count();
            var fhsKom = komListe.Count(Resultatrapport => Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == Resultatrapport.stemmesedler.total);
            var vtsKom = komListe.Count(Resultatrapport => Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == 0);
            var altKom = komListe.Count(Resultatrapport => Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs != Resultatrapport.stemmesedler.total);
            var alt8Kom = komListe.Count(Resultatrapport => Resultatrapport.opptalt.endelig == 100);

            //Use StatusInd for krets count logic
            var totKrets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets");
            var fhsKrets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && (Resultatrapport.StatusIndikator == 1 || Resultatrapport.StatusIndikator == 2));
            var vtsKrets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && (Resultatrapport.StatusIndikator == 3 || Resultatrapport.StatusIndikator == 4));
            var altKrets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && Resultatrapport.StatusIndikator >= 5);
            var alt8Krets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && Resultatrapport.StatusIndikator == 8);

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKomm", Value = Convert.ToString(totKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommFhstOpptalt", Value = Convert.ToString(fhsKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommVtstOpptalt", Value = Convert.ToString(vtsKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommAltOpptalt", Value = Convert.ToString(altKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommAltOppt8", Value = Convert.ToString(alt8Kom) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKretser", Value = Convert.ToString(totKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserFhstOpptalt", Value = Convert.ToString(fhsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserVtstOpptalt", Value = Convert.ToString(vtsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOpptalt", Value = Convert.ToString(altKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOppt8", Value = Convert.ToString(alt8Krets) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntStBerett", Value = Convert.ToString(reportCache[rootRef].antallsb) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFrammotte", Value = Convert.ToString(reportCache[rootRef].stemmesedler.total) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProFrammotte", Value = Tools.FormatDecimal(Math.Max(reportCache[rootRef].frammote.prosent.GetValueOrDefault(), reportCache[rootRef].frammote.prosentStemmegiving.GetValueOrDefault())) });

            //Top 10
            var tabell = new responsRapportTabell() { navn = "K07tabell1" };
            foreach (var item in komListe.Where(x => x.frammote.prosent.HasValue).OrderByDescending(y => Math.Max(y.frammote.prosent.Value, y.frammote.prosentStemmegiving.GetValueOrDefault())).Take(10))
            {
                var liste = new responsRapportTabellListe();

                liste.data.Add(new responsRapportTabellListeData() { navn = "KommNavnHoy", Value = item.lenker.self.navn });
                liste.data.Add(new responsRapportTabellListeData() { navn = "FylkeNavnHoy", Value = item.lenker.up.navn });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProFrammotteHoy", Value = Tools.FormatDecimal(Math.Max(item.frammote.prosent.Value, item.frammote.prosentStemmegiving.GetValueOrDefault())) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFrammFKsvHoy", Value = Tools.FormatDecimal(item.frammote.endring, true) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFrammFStvHoy", Value = "-" });
                liste.tabell = null;
                tabell.liste.Add(liste);
            }
            doc.firstRapport.tabell.Add(tabell);

            //Bottom 10
            tabell = new responsRapportTabell() { navn = "K07tabell2" };
            foreach (var item in komListe.Where(x => x.frammote.prosent.HasValue).OrderBy(y => Math.Max(y.frammote.prosent.Value, y.frammote.prosentStemmegiving.GetValueOrDefault())).Take(10))
            {
                var liste = new responsRapportTabellListe();

                liste.data.Add(new responsRapportTabellListeData() { navn = "KommNavnLav", Value = item.lenker.self.navn });
                liste.data.Add(new responsRapportTabellListeData() { navn = "FylkeNavnLav", Value = item.lenker.up.navn });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProFrammotteLav", Value = Tools.FormatDecimal(Math.Max(item.frammote.prosent.Value, item.frammote.prosentStemmegiving.GetValueOrDefault())) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFrammFKsvLav", Value = Tools.FormatDecimal(item.frammote.endring, true) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFrammFStvLav", Value = "-" });
                liste.tabell = null;
                tabell.liste.Add(liste);
            }
            doc.firstRapport.tabell.Add(tabell);
            return doc;

        }

        //Bystyre - Oslo
        public static respons GetK09(Dictionary<string, Resultatrapport> reportCache, string rootRef)
        {
            //Pick the actual report node
            var report = reportCache[rootRef];

            //And download a FY helper
            string json = Helpers.Tools.DoQuery(rootRef.Replace("ko", "fy").Replace("/0301", ""));
            //var fy_report = JsonConvert.DeserializeObject<Resultatrapport>(json);

            //Start with blank new XML-report
            var doc = XmlHelpers.GetNewXMLReport("K09", reportCache[rootRef].tidspunkt.rapportGenerert);

            //Actuall XML generation
            doc.firstRapport.data.Add(new responsRapportData() { navn = "KommNr", Value = report.lenker.up.nr });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "KommNavn", Value = report.lenker.up.navn });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntStBerett", Value = Convert.ToString(report.antallsb) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFrammotte", Value = Convert.ToString(report.stemmesedler.total) });

            //Use Stemmegiving for kretser, will give a reasonable value when frammøte is missing
            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProFrammotte", Value = Tools.FormatDecimal(Math.Max(reportCache[rootRef].frammote.prosent.GetValueOrDefault(), reportCache[rootRef].frammote.prosentStemmegiving.GetValueOrDefault())) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFhstOpptalt", Value = Tools.FormatDecimal(report.stemmesedler.fhs) });

            //Looping parti-liste data
            var tabell = new responsRapportTabell() { navn = "K09tabell1" };
            foreach (var item in report.partier)
            {
                var liste = new responsRapportTabellListe();
                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikode", Value = item.partiNavn.partikode });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntFhst", Value = Convert.ToString(item.stemmetall.resultat.antall.fhs) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntStemmer", Value = Convert.ToString(item.stemmetall.resultat.antall.total) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProSt", Value = Tools.FormatDecimal(item.stemmetall.resultat.prosent) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFKsv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.samme, true) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFStv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.ekvivalent, true) });

                //Prognosetall if avail
                //if (report.prognose.beregnet)
                //{
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgProSt", Value = Tools.FormatDecimal(item.stemmetall.prognose.prosent) });
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFKsv", Value = Tools.FormatDecimal(item.stemmetall.prognose.endring.samme, true) });
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFStv", Value = Tools.FormatDecimal(item.stemmetall.prognose.endring.ekvivalent, true) });

                //    //Fylkesmandater
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndt", Value = Convert.ToString(item.mandattall.prognose.antall) });
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndt", Value = Tools.AddSign(item.mandattall.prognose.endring) });
                //}
                if (item.mandattall != null)
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgProSt", Value = "-" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFFtv", Value = "-" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFStv", Value = "-" });

                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndt", Value = Convert.ToString(item.mandattall.resultat.antall) });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndt", Value = Tools.AddSign(item.mandattall.resultat.endring) });
                }
                else
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgProSt", Value = "-" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFFtv", Value = "-" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFStv", Value = "-" });

                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndt" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndt" });
                }

                //Prognosetall from FY report if avail
                //var parti = fy_report.partier.First(x => x.partiNavn.partikode == item.partiNavn.partikode);
                //if (fy_report.prognose.beregnet)
                //{
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStv", Value = Convert.ToString(parti.mandattall.prognose.ekvivalent.antall) });
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndtFStv", Value = Tools.AddSign(parti.mandattall.prognose.ekvivalent.endring) });
                //}

                ////Extra check on mandatattall needed to handle certain cases. Appears that mandater for KO at fylkeslevel takes a while to appear
                //else if (parti.mandattall != null)
                //{
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStv", Value = Convert.ToString(parti.mandattall.resultat.ekvivalent.antall) });
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndtFStv", Value = Tools.AddSign(parti.mandattall.resultat.ekvivalent.endring) });
                //}
                //else
                //{
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStv" });
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndtFStv" });
                //}

                //For the 2019 Election these ST-simulated fields will _not_have data, due to the region reform, commented out and added empty below
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStv" });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndtFStv" });

                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikategori", Value = Convert.ToString(item.partiNavn.partikategori) });
                liste.tabell = null;
                tabell.liste.Add(liste);
            }

            doc.firstRapport.tabell.Add(tabell);
            return doc;
        }

    }
    
    class XmlHelpersFY
    {
        //Land - Stortingsoversikt/prognose
        public static respons GetF07(Dictionary<string, Resultatrapport> reportCache, string rootRef)
        {
            //Start with blank new XML-report based on root
            var doc = XmlHelpers.GetNewXMLReport("F07", reportCache[rootRef].tidspunkt.rapportGenerert);

            //Not using calculated StatusInd here, but same logic is applied, handle Oslo not being/having a complete dataset fro FY valg
            var totKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr != "0301");
            var fhsKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr != "0301" &&
                Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == Resultatrapport.stemmesedler.total);
            var vtsKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr != "0301" &&
                Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == 0);
            var altKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr != "0301" &&
                Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs != Resultatrapport.stemmesedler.total);
            var alt8Kom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr != "0301" && Resultatrapport.opptalt.endelig == 100);

            //Use StatusInd for krets count logic, no need to handle Oslo here as these data are not present for this level for FY
            var totKrets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets");
            var fhsKrets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && (Resultatrapport.StatusIndikator == 1 || Resultatrapport.StatusIndikator == 2));
            var vtsKrets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && (Resultatrapport.StatusIndikator == 3 || Resultatrapport.StatusIndikator == 4));
            var altKrets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && Resultatrapport.StatusIndikator >= 5);
            var alt8Krets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && Resultatrapport.StatusIndikator == 8);

            //Actuall XML generation
            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKommL", Value = Convert.ToString(totKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommFhstOpptaltL", Value = Convert.ToString(fhsKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommVtstOpptaltL", Value = Convert.ToString(vtsKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommAltOpptaltL", Value = Convert.ToString(altKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommAltOppt8L", Value = Convert.ToString(alt8Kom) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKretserL", Value = Convert.ToString(totKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserFhstOpptaltL", Value = Convert.ToString(fhsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserVtstOpptaltL", Value = Convert.ToString(vtsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOpptaltL", Value = Convert.ToString(altKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOppt8L", Value = Convert.ToString(alt8Krets) });

            //doc.firstRapport.data.Add(new responsRapportData() { navn = "StDevLand", Value = (reportCache[rootRef].prognose.beregnet ? Tools.FormatDecimal(reportCache[rootRef].prognose.historiskTotalavvik) : "0") });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "StDevLand", Value = "0" });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntStBerettL", Value = Convert.ToString(reportCache[rootRef].antallsb) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFrammotteL", Value = Convert.ToString(reportCache[rootRef].stemmesedler.total) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProFrammotteL", Value = Tools.FormatDecimal(reportCache[rootRef].frammote.prosent) });


            //Fylkesliste based on fylkesrapporter
            var tabell = new responsRapportTabell() { navn = "F07tabell1" };
            foreach (var f in reportCache.Where(x => x.Value.rapportId.nivaa == "fylke").Select(r => r.Value))
            {
                //Per fylke calculations    
                //Not using calculated StatusInd here, but same logic is applied
                totKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(f.rapportId.nr));
                fhsKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(f.rapportId.nr) &&
                    Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == Resultatrapport.stemmesedler.total);
                vtsKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(f.rapportId.nr) &&
                    Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == 0);
                altKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(f.rapportId.nr) &&
                    Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs != Resultatrapport.stemmesedler.total);
                alt8Kom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(f.rapportId.nr) &&
                    Resultatrapport.opptalt.endelig == 100);

                //Apply krets count to relevant selection only, extra selector needed as krets-numbers does not include fylke/kommune numbers. Use StatusInd for logic
                var kretser = reportCache.Where(Resultatrapport => Resultatrapport.Value.rapportId.nivaa == "stemmekrets" && Resultatrapport.Key.StartsWith(f.lenker.self.href)).Select(rr => rr.Value);
                totKrets = kretser.Count();
                fhsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 1 || Resultatrapport.StatusIndikator == 2);
                vtsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 3 || Resultatrapport.StatusIndikator == 4);
                altKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator >= 5);
                alt8Krets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 8);

                //Conditional indicators, sistemandat
                double? tapeStemmer = null;
                double? vinneStemmer = null;
                string tapeParti = string.Empty;
                string vinneParti = string.Empty;

                //TODO: FY 2023 - Miste/vinne numbers are no longer possible. Check logic
                //Extra check on mandatattall needed to handle certain cases
                //if (f.prognose.beregnet && f.partier[0].mandattall != null)
                //{
                //    tapeStemmer = f.partier.Select(Parti => Parti.mandattall.prognose.sisteMandat.kvotient).Min();
                //    vinneStemmer = f.partier.Select(Parti => Parti.mandattall.prognose.nesteMandat.kvotient).Max();

                //    tapeParti = f.partier.First(x => x.mandattall.prognose.sisteMandat.kvotient == tapeStemmer).partiNavn.partikode;
                //    vinneParti = f.partier.First(x => x.mandattall.prognose.nesteMandat.kvotient == vinneStemmer).partiNavn.partikode;
                //}
                if (f.partier[0].mandattall != null)
                {
                    tapeStemmer = f.partier.Select(Parti => Parti.mandattall.resultat.sisteMandat.kvotient).Min();
                    vinneStemmer = f.partier.Select(Parti => Parti.mandattall.resultat.nesteMandat.kvotient).Max();

                    tapeParti = f.partier.First(x => x.mandattall.resultat.sisteMandat.kvotient == tapeStemmer).partiNavn.partikode;
                    vinneParti = f.partier.First(x => x.mandattall.resultat.nesteMandat.kvotient == vinneStemmer).partiNavn.partikode;
                }

                var liste = new responsRapportTabellListe();
                liste.data.Add(new responsRapportTabellListeData() { navn = "FylkeNr", Value = f.rapportId.nr });
                liste.data.Add(new responsRapportTabellListeData() { navn = "FylkeNavn", Value = f.rapportId.navn });

                liste.data.Add(new responsRapportTabellListeData() { navn = "TotAntKomm", Value = Convert.ToString(totKom) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntKommFhstOpptalt", Value = Convert.ToString(fhsKom) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntKommVtstOpptalt", Value = Convert.ToString(vtsKom) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntKommAltOpptalt", Value = Convert.ToString(altKom) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntKommAltOppt8", Value = Convert.ToString(alt8Kom) });

                liste.data.Add(new responsRapportTabellListeData() { navn = "TotAntKretser", Value = Convert.ToString(totKrets) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntKretserFhstOpptalt", Value = Convert.ToString(fhsKrets) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntKretserVtstOpptalt", Value = Convert.ToString(vtsKrets) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntKretserAltOpptalt", Value = Convert.ToString(altKrets) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntKretserAltOppt8", Value = Convert.ToString(alt8Krets) });

                //liste.data.Add(new responsRapportTabellListeData() { navn = "StDevFylke", Value = (f.prognose.beregnet ? Tools.FormatDecimal(f.prognose.historiskTotalavvik) : "0") });
                liste.data.Add(new responsRapportTabellListeData() { navn = "StDevFylke", Value = "0" });

                liste.data.Add(new responsRapportTabellListeData() { navn = "AntStBerett", Value = Convert.ToString(f.antallsb) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntFrammotte", Value = Convert.ToString(f.stemmesedler.total) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProFrammotte", Value = Tools.FormatDecimal(f.frammote.prosent) });

                liste.data.Add(new responsRapportTabellListeData() { navn = "AntFhstOpptalt", Value = Convert.ToString(f.stemmesedler.fhs) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProgProStOpptalt", Value = Tools.FormatDecimal(f.opptalt.prosent) });

                liste.data.Add(new responsRapportTabellListeData() { navn = "PartikodeMiste", Value = tapeParti });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntStMiste", Value = "" });
                liste.data.Add(new responsRapportTabellListeData() { navn = "PartikodeVinne", Value = vinneParti });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntStVinne", Value = "" });

                //Loop partier per fylke
                //TODO: FY 2023 - Fall back to real numbers when prognose is not available in this table as well?
                liste.tabell = new responsRapportTabellListeTabell() { navn = "F07tabell11" };
                foreach (var p in f.partier)
                {
                    var liste2 = new responsRapportTabellListeTabellListe();
                    liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "Partikode", Value = p.partiNavn.partikode });
                    liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "AntFhst", Value = Convert.ToString(p.stemmetall.resultat.antall.fhs) });
                    liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "AntStemmer", Value = Convert.ToString(p.stemmetall.resultat.antall.total) });

                    //Extra check on mandatattall needed to handle certain cases
                    //if (f.prognose.beregnet && f.partier[0].mandattall != null)
                    //{
                    //    liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgProSt", Value = Tools.FormatDecimal(p.stemmetall.prognose.prosent) });
                    //    liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "DiffProgPropFFtv", Value = Tools.FormatDecimal(p.stemmetall.prognose.endring.samme, true) });
                    //    liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "DiffProgPropFStv", Value = Tools.FormatDecimal(p.stemmetall.prognose.endring.ekvivalent, true) });
                    //    liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgAntMndtFtv", Value = Convert.ToString(p.mandattall.prognose.antall) });
                    //    liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "DiffProgAntMndtFFtv", Value = Tools.AddSign(p.mandattall.prognose.endring) });
                    //    liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgAntMndtStv", Value = Convert.ToString(p.mandattall.prognose.ekvivalent.antall) });

                    //    //Win/Loose indicator flags, sistemandat - These might be wrong, using number for fylkesmandtaer to calculate simulated ST election here 
                    //    if (p.mandattall.prognose.sisteMandat.kvotient == tapeStemmer)
                    //        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgVinneMisteStvInd", Value = "-" });
                    //    else if (p.mandattall.prognose.nesteMandat.kvotient == vinneStemmer)
                    //        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgVinneMisteStvInd", Value = "+" });
                    //    else
                    //        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgVinneMisteStvInd" });

                    //    liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgUtjMndtStv", Value = Convert.ToString(p.mandattall.prognose.ekvivalent.utjevningAntall) });
                    //}
                    if (f.partier[0].mandattall != null)
                    {
                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgProSt", Value = Tools.FormatDecimal(p.stemmetall.resultat.prosent) });
                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "DiffProgPropFFtv", Value = Tools.FormatDecimal(p.stemmetall.resultat.endring.samme, true) });
                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "DiffProgPropFStv", Value = Tools.FormatDecimal(p.stemmetall.resultat.endring.ekvivalent, true) });
                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgAntMndtFtv", Value = Convert.ToString(p.mandattall.resultat.antall) });
                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "DiffProgAntMndtFFtv", Value = Tools.AddSign(p.mandattall.resultat.endring) });
                        //liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgAntMndtStv", Value = Convert.ToString(p.mandattall.resultat.ekvivalent.antall) });
                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgAntMndtStv", Value = "" });
                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgVinneMisteStvInd", Value = "" });
                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgUtjMndtStv", Value = "" });
                    }
                    else //if (f.partier[0].mandattall != null)
                    {
                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgProSt", Value = "-" });
                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "DiffProgPropFFtv", Value = "-" });
                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "DiffProgPropFStv", Value = "-" });
                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgAntMndtFtv", Value = "-" });
                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "DiffProgAntMndtFFtv", Value = "-" });
                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgAntMndtStv", Value = "-" });
                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgVinneMisteStvInd", Value = "" });
                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgUtjMndtStv", Value = "-" });
                    }
                    liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "Partikategori", Value = Convert.ToString(p.partiNavn.partikategori) });
                    liste.tabell.liste.Add(liste2);
                }
                tabell.liste.Add(liste);
            }
            doc.firstRapport.tabell.Add(tabell);

            //Mandater per parti based on st-root
            tabell = new responsRapportTabell() { navn = "F07tabell2" };
            foreach (var m in reportCache[rootRef].partier.Where(x => x.partiNavn.navn != "Andre2"))
            {
                var liste = new responsRapportTabellListe();
                liste.data.Add(new responsRapportTabellListeData() { navn = "PartikodeL", Value = m.partiNavn.partikode });

                //TODO: FY 2023 - Fall back to real numbers when prognose is not available in this table as well?
                //if (reportCache[rootRef].prognose.beregnet && m.mandattall != null)
                //{
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndFtvL", Value = Convert.ToString(m.mandattall.prognose.antall) });

                //    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntDMndtStvL", Value = Convert.ToString(m.mandattall.prognose.ekvivalent.antall - m.mandattall.prognose.ekvivalent.utjevningAntall) });
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntUMndtStvL", Value = Convert.ToString(m.mandattall.prognose.ekvivalent.utjevningAntall) });
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStvL", Value = Convert.ToString(m.mandattall.prognose.ekvivalent.antall) });
                //}
                if (m.mandattall != null)
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndFtvL", Value = Convert.ToString(m.mandattall.resultat.antall) });

                    //liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntDMndtStvL", Value = Convert.ToString(m.mandattall.resultat.ekvivalent.antall - m.mandattall.resultat.ekvivalent.utjevningAntall) });
                    //liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntUMndtStvL", Value = Convert.ToString(m.mandattall.resultat.ekvivalent.utjevningAntall) });
                    //liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStvL", Value = Convert.ToString(m.mandattall.resultat.ekvivalent.antall) });

                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntDMndtStvL" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntUMndtStvL" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStvL" });
                }
                else
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndFtvL" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntDMndtStvL" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntUMndtStvL" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStvL" });
                }

                liste.data.Add(new responsRapportTabellListeData() { navn = "PartikategoriL", Value = Convert.ToString(m.partiNavn.partikategori) });
                liste.tabell = null;
                tabell.liste.Add(liste);
            }
            doc.firstRapport.tabell.Add(tabell);
            return doc;
        }

        //Land
        public static respons GetF05(Resultatrapport report, Dictionary<string, Resultatrapport> reportCache)
        {
            //Start with blank new XML-report
            var doc = XmlHelpers.GetNewXMLReport("F05", report.tidspunkt.rapportGenerert);

            //Not using calculated StatusInd here, but same logic is applied, handle Oslo not being/having a complete dataset fro FY valg
            var totKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr != "0301");
            var fhsKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr != "0301" &&
                Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == Resultatrapport.stemmesedler.total);
            var vtsKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr != "0301" &&
                Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == 0);
            var altKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr != "0301" &&
                Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs != Resultatrapport.stemmesedler.total);
            var alt8Kom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr != "0301" && Resultatrapport.opptalt.endelig == 100);

            //Use StatusInd for krets count logic, no need to handlr Oslo here as these data are not present for this level for FY
            var totKrets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets");
            var fhsKrets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && (Resultatrapport.StatusIndikator == 1 || Resultatrapport.StatusIndikator == 2));
            var vtsKrets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && (Resultatrapport.StatusIndikator == 3 || Resultatrapport.StatusIndikator == 4));
            var altKrets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && Resultatrapport.StatusIndikator >= 5);
            var alt8Krets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && Resultatrapport.StatusIndikator == 8);

            //Actuall XML generation
            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKomm", Value = Convert.ToString(totKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommFhstOpptalt", Value = Convert.ToString(fhsKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommVtstOpptalt", Value = Convert.ToString(vtsKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommAltOpptalt", Value = Convert.ToString(altKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommAltOppt8", Value = Convert.ToString(alt8Kom) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKretser", Value = Convert.ToString(totKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserFhstOpptalt", Value = Convert.ToString(fhsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserVtstOpptalt", Value = Convert.ToString(vtsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOpptalt", Value = Convert.ToString(altKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOppt8", Value = Convert.ToString(alt8Krets) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProgProStOpptalt", Value = Tools.FormatDecimal(report.opptalt.prosent) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntStBerett", Value = Convert.ToString(report.antallsb) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFrammotte", Value = Convert.ToString(report.stemmesedler.total) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProFrammotte", Value = Tools.FormatDecimal(report.frammote.prosent) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFhstOpptalt", Value = Convert.ToString(report.stemmesedler.fhs) });

            //Looping parti-liste data
            var tabell = new responsRapportTabell() { navn = "F05tabell1" };
            foreach (var item in report.partier)
            {
                var liste = new responsRapportTabellListe();
                //Opptalt data7
                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikode", Value = item.partiNavn.partikode });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntFhst", Value = Convert.ToString(item.stemmetall.resultat.antall.fhs) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntStemmer", Value = Convert.ToString(item.stemmetall.resultat.antall.total) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProSt", Value = Tools.FormatDecimal(item.stemmetall.resultat.prosent) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFFtv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.samme, true) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFStv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.ekvivalent, true) });

                //Prognosetall if avail
                //if (report.prognose.beregnet)
                //{
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgProSt", Value = Tools.FormatDecimal(item.stemmetall.prognose.prosent) });
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFFtv", Value = Tools.FormatDecimal(item.stemmetall.prognose.endring.samme, true) });
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFStv", Value = Tools.FormatDecimal(item.stemmetall.prognose.endring.ekvivalent, true) });
                //}
                //else
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgProSt", Value = "-" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFFtv", Value = "-" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFStv", Value = "-" });
                }

                //Mandat-tall either from prognose or real segment
                //TODO: FY 2023 - Do we get diff numbers from 2021 ST now?
                //if (report.prognose.beregnet && item.mandattall != null)
                //{
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtFtv", Value = Convert.ToString(item.mandattall.prognose.antall) });
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndtFtv", Value = Tools.AddSign(item.mandattall.prognose.endring) });
                //    //liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStv", Value = Convert.ToString(item.mandattall.prognose.ekvivalent.antall) });
                //    //liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndtStv", Value = Tools.AddSign(item.mandattall.prognose.ekvivalent.endring) });
                //}
                if (item.mandattall != null)
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtFtv", Value = Convert.ToString(item.mandattall.resultat.antall) });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndtFtv", Value = Tools.AddSign(item.mandattall.resultat.endring) });
                    //liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStv", Value = Convert.ToString(item.mandattall.resultat.ekvivalent.antall) });
                    //liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndtStv", Value = Tools.AddSign(item.mandattall.resultat.ekvivalent.endring) });
                }
                else
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtFtv", Value = "0" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndtFtv", Value = "0" });
                    //liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStv", Value = "0" });
                    //liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndtStv", Value = "0" });
                }

                //TODO: FY 2023 - For the 2019 Election these ST-simulated fields will _not_have data, due to the region reform, commented out above and added here
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStv", Value = "" });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndtStv", Value = "" });


                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikategori", Value = Convert.ToString(item.partiNavn.partikategori) });
                liste.tabell = null;
                tabell.liste.Add(liste);
            }

            doc.firstRapport.tabell.Add(tabell);
            return doc;
        }

        //Fylke
        public static respons GetF04(Resultatrapport report, Dictionary<string, Resultatrapport> reportCache)
        {
            //Start with blank new XML-report
            var doc = XmlHelpers.GetNewXMLReport("F04", report.tidspunkt.rapportGenerert);

            //Misc calculations
            int totKom = 0;
            int fhsKom = 0;
            int vtsKom = 0;
            int altKom = 0;
            int alt8Kom = 0;

            int totKrets = 0;
            int fhsKrets = 0;
            int vtsKrets = 0;
            int altKrets = 0;
            int alt8Krets = 0;

            //Handle Oslo / 0301 as it lacks complete FY dataset, nor actually being a Fylke
            //Sumply set counts to '1' appropriately
            if (report.rapportId.nr == "03")
            {
                //Get a proper StatusInd based on Oslo FY level 
                var sInd = Helpers.Tools.GetStatusIndicator(report);
                
                //Set coorect values to '1'
                totKom = 1;
                if (sInd == 1 || sInd == 2)
                    fhsKom = 1;
                else if (sInd == 3 || sInd == 4)
                    vtsKom = 1;
                else if (sInd >=5 )
                    altKom = 1;
                if (sInd == 8)
                    alt8Kom = 1;
            }
            else
            {
                //Not using calculated StatusInd here, but same logic is applied
                totKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr));
                fhsKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr) &&
                    Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == Resultatrapport.stemmesedler.total);
                vtsKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr) &&
                    Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == 0);
                altKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr) &&
                    Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs != Resultatrapport.stemmesedler.total);
                alt8Kom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr) &&
                    Resultatrapport.opptalt.endelig == 100);

                //Apply krets count to relevant selection only, extra selector needed as krets-numbers does not include fylke/kommune numbers. Use StatusInd for logic
                var kretser = reportCache.Where(Resultatrapport => Resultatrapport.Value.rapportId.nivaa == "stemmekrets" && Resultatrapport.Key.StartsWith(report.lenker.self.href)).Select(rr => rr.Value);
                totKrets = kretser.Count();
                fhsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 1 || Resultatrapport.StatusIndikator == 2);
                vtsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 3 || Resultatrapport.StatusIndikator == 4);
                altKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator >= 5);
                alt8Krets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 8);
            }

            //Conditional indicators, sistemandat
            double? tapeStemmer = null;
            double? vinneStemmer = null;
            string tapeParti = string.Empty;
            string vinneParti = string.Empty;

            //TODO: FY 2023 - Miste/vinne numbers are no longer possible. Check logic
            //Extra check on mandatattall needed to handle certain cases
            //if (report.prognose.beregnet && report.partier[0].mandattall != null)
            //{
            //    tapeStemmer = report.partier.Select(Parti => Parti.mandattall.prognose.sisteMandat.kvotient).Min();
            //    vinneStemmer = report.partier.Select(Parti => Parti.mandattall.prognose.nesteMandat.kvotient).Max();

            //    tapeParti = report.partier.First(x => x.mandattall.prognose.sisteMandat.kvotient == tapeStemmer).partiNavn.partikode;
            //    vinneParti = report.partier.First(x => x.mandattall.prognose.nesteMandat.kvotient == vinneStemmer).partiNavn.partikode;
            //}
            if (report.partier[0].mandattall != null)
            {
                tapeStemmer = report.partier.Select(Parti => Parti.mandattall.resultat.sisteMandat.kvotient).Min();
                vinneStemmer = report.partier.Select(Parti => Parti.mandattall.resultat.nesteMandat.kvotient).Max();

                tapeParti = report.partier.First(x => x.mandattall.resultat.sisteMandat.kvotient == tapeStemmer).partiNavn.partikode;
                vinneParti = report.partier.First(x => x.mandattall.resultat.nesteMandat.kvotient == vinneStemmer).partiNavn.partikode;
            }

            //Actuall XML generation
            doc.firstRapport.data.Add(new responsRapportData() { navn = "FylkeNr", Value = report.rapportId.nr });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "FylkeNavn", Value = report.rapportId.navn });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKomm", Value = Convert.ToString(totKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommFhstOpptalt", Value = Convert.ToString(fhsKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommVtstOpptalt", Value = Convert.ToString(vtsKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommAltOpptalt", Value = Convert.ToString(altKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommAltOppt8", Value = Convert.ToString(alt8Kom) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKretser", Value = Convert.ToString(totKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserFhstOpptalt", Value = Convert.ToString(fhsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserVtstOpptalt", Value = Convert.ToString(vtsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOpptalt", Value = Convert.ToString(altKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOppt8", Value = Convert.ToString(alt8Krets) });

            //doc.firstRapport.data.Add(new responsRapportData() { navn = "StDevFylke", Value = (report.prognose.beregnet ? Tools.FormatDecimal(report.prognose.historiskTotalavvik) : "0") });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "StDevFylke", Value = "0" });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProgProStOpptalt", Value = Tools.FormatDecimal(report.opptalt.prosent) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntStBerett", Value = Convert.ToString(report.antallsb) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFrammotte", Value = Convert.ToString(report.stemmesedler.total) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProFrammotte", Value = Tools.FormatDecimal(report.frammote.prosent) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFhstOpptalt", Value = Convert.ToString(report.stemmesedler.fhs) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "PartikodeMiste", Value = tapeParti });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntStMiste", Value = "" });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "PartikodeVinne", Value = vinneParti });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntStVinne", Value = "" });

            //Looping parti-liste data
            var tabell = new responsRapportTabell() { navn = "F04tabell1" };
            foreach (var item in report.partier)
            {
                var liste = new responsRapportTabellListe();
                //Opptalt data
                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikode", Value = item.partiNavn.partikode });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntFhst", Value = Convert.ToString(item.stemmetall.resultat.antall.fhs) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntStemmer", Value = Convert.ToString(item.stemmetall.resultat.antall.total) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProSt", Value = Tools.FormatDecimal(item.stemmetall.resultat.prosent) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFFtv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.samme, true) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFStv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.ekvivalent, true) });

                ////Prognosetall if avail
                //if (report.prognose.beregnet)
                //{
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgProSt", Value = Tools.FormatDecimal(item.stemmetall.prognose.prosent) });
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFFtv", Value = Tools.FormatDecimal(item.stemmetall.prognose.endring.samme, true) });
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFStv", Value = Tools.FormatDecimal(item.stemmetall.prognose.endring.ekvivalent, true) });
                //}
                //else
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgProSt", Value = "-" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFFtv", Value = "-" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFStv", Value = "-" });
                }

                //Mandat-tall either from prognose or real segment
                //Extra check on mandatattall needed to handle certain cases
                //if (report.prognose.beregnet && item.mandattall != null)
                //{
                //    //Fylkesmandater
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtFtv", Value = Convert.ToString(item.mandattall.prognose.antall) });
                //    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndtFFtv", Value = Tools.AddSign(item.mandattall.prognose.endring) });

                //    //Stortingsmandater, prognostisert ST-valg
                //    //liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStv", Value = Convert.ToString(item.mandattall.prognose.ekvivalent.antall) });
                //    //liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndtFStv", Value = Tools.AddSign(item.mandattall.prognose.ekvivalent.endring) });

                //    ////Win/Loose indicator flags, sistemandat
                //    //if (item.mandattall.resultat.sisteMandat.kvotient == tapeStemmer)
                //    //    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgVinneMisteStvInd", Value = "-" });
                //    //else if (item.mandattall.resultat.nesteMandat.kvotient == vinneStemmer)
                //    //    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgVinneMisteStvInd", Value = "+" });
                //    //else
                //    //    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgVinneMisteStvInd" });

                //    //liste.data.Add(new responsRapportTabellListeData() { navn = "ProgUtjMndtStv", Value = Convert.ToString(item.mandattall.prognose.ekvivalent.utjevningAntall) });

                //}
                if (item.mandattall != null)
                {
                    //Fylkesmandater
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtFtv", Value = Convert.ToString(item.mandattall.resultat.antall) });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndtFFtv", Value = Tools.AddSign(item.mandattall.resultat.endring) });

                    //Stortingsmandater, prognostisert ST-valg
                    //liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStv", Value = Convert.ToString(item.mandattall.resultat.ekvivalent.antall) });
                    //liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndtFStv", Value = Tools.AddSign(item.mandattall.resultat.ekvivalent.endring) });

                    ////Win/Loose indicator flags, sistemandat
                    //if (item.mandattall.resultat.sisteMandat.kvotient == tapeStemmer)
                    //    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgVinneMisteStvInd", Value = "-" });
                    //else if (item.mandattall.resultat.nesteMandat.kvotient == vinneStemmer)
                    //    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgVinneMisteStvInd", Value = "+" });
                    //else
                    //    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgVinneMisteStvInd" });

                    //liste.data.Add(new responsRapportTabellListeData() { navn = "ProgUtjMndtStv", Value = Convert.ToString(item.mandattall.resultat.ekvivalent.utjevningAntall) });
                }
                else
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtFtv", Value = "0" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndtFFtv", Value = "0" });
                    //liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStv", Value = "0" });
                    //liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndtFStv", Value = "0" });
                    //liste.data.Add(new responsRapportTabellListeData() { navn = "ProgVinneMisteStvInd" });
                    //liste.data.Add(new responsRapportTabellListeData() { navn = "ProgUtjMndtStv", Value = "0" });

                }

                //TODO: FY 2023 - For the 2019 Election these ST-simulated fields will _not_have data, due to the region reform, commented out above and added here
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStv", Value = "" });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndtFStv", Value = "" });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProgVinneMisteStvInd", Value = ""});
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProgUtjMndtStv", Value = "" });


                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikategori", Value = Convert.ToString(item.partiNavn.partikategori) });
                liste.tabell = null;
                tabell.liste.Add(liste);
            }

            doc.firstRapport.tabell.Add(tabell);

            return doc;
        }

        //Krets
        public static respons GetF03(Resultatrapport report, Dictionary<string, Resultatrapport> reportCache)
        {
            //Start with blank new XML-report
            var doc = XmlHelpers.GetNewXMLReport("F03", report.tidspunkt.rapportGenerert);

            //Determine correct aggregation parent
            var parent = reportCache[report.lenker.up.href].rapportId.nivaa == "bydel" ? reportCache[report.lenker.up.href].lenker.up : report.lenker.up;

            //Apply krets count to relevant selection only, extra selector needed as krets-numbers does not include fylke/kommune numbers. Use StatusInd for logic.
            var kretser = reportCache.Where(Resultatrapport => Resultatrapport.Value.rapportId.nivaa == "stemmekrets" &&
                Resultatrapport.Key.StartsWith(parent.href)).Select(rr => rr.Value);
            var totKrets = kretser.Count();
            var fhsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 1 || Resultatrapport.StatusIndikator == 2);
            var vtsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 3 || Resultatrapport.StatusIndikator == 4);
            var altKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator >= 5);
            var alt8Krets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 8);

            //Actuall XML generation
            doc.firstRapport.data.Add(new responsRapportData() { navn = "KommNr", Value = parent.nr });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "KommNavn", Value = parent.navn });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKretser", Value = Convert.ToString(totKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserFhstOpptalt", Value = Convert.ToString(fhsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserVtstOpptalt", Value = Convert.ToString(vtsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOpptalt", Value = Convert.ToString(altKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOppt8", Value = Convert.ToString(alt8Krets) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "KretsNr", Value = report.rapportId.nr });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "KretsNavn", Value = report.rapportId.navn });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "StatusInd", Value = Convert.ToString(report.StatusIndikator) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntStBerett", Value = Convert.ToString(report.antallsb) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFrammotte", Value = Convert.ToString(report.stemmesedler.total) });

            //Use Stemmegiving for kretser, will give a reasonable value when frammøte is missing
            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProFrammotte", Value = Tools.FormatDecimal(report.frammote.prosentStemmegiving) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFhstOpptalt", Value = Convert.ToString(report.stemmesedler.fhs) });

            //Looping parti-liste data
            var tabell = new responsRapportTabell() { navn = "F03tabell1" };
            foreach (var item in report.partier)
            {
                var liste = new responsRapportTabellListe();
                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikode", Value = item.partiNavn.partikode });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntFhst", Value = Convert.ToString(item.stemmetall.resultat.antall.fhs) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntStemmer", Value = Convert.ToString(item.stemmetall.resultat.antall.total) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProSt", Value = Tools.FormatDecimal(item.stemmetall.resultat.prosent) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFFtv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.samme, true) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFStv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.ekvivalent, true) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikategori", Value = Convert.ToString(item.partiNavn.partikategori) });
                liste.tabell = null;
                tabell.liste.Add(liste);
            }

            doc.firstRapport.tabell.Add(tabell);
            return doc;
        }

        //Kommune
        public static respons GetF02(Resultatrapport report, Dictionary<string, Resultatrapport> reportCache)
        {
            //Start with blank new XML-report
            var doc = XmlHelpers.GetNewXMLReport("F02", report.tidspunkt.rapportGenerert);

            //Apply krets count to relevant selection only, extra selector needed as krets-numbers does not include fylke/kommune numbers. Use StatusInd for logic.
            var kretser = reportCache.Where(Resultatrapport => Resultatrapport.Value.rapportId.nivaa == "stemmekrets" &&
                Resultatrapport.Key.StartsWith(report.lenker.self.href)).Select(rr => rr.Value);
            var totKrets = kretser.Count();
            var fhsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 1 || Resultatrapport.StatusIndikator == 2);
            var vtsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 3 || Resultatrapport.StatusIndikator == 4);
            var altKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator >= 5);
            var alt8Krets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 8);

            //Actuall XML generation
            doc.firstRapport.data.Add(new responsRapportData() { navn = "FylkeNr", Value = report.lenker.up.nr });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "FylkeNavn", Value = report.lenker.up.navn });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "KommNr", Value = report.rapportId.nr });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "KommNavn", Value = report.rapportId.navn });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKretser", Value = Convert.ToString(totKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserFhstOpptalt", Value = Convert.ToString(fhsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserVtstOpptalt", Value = Convert.ToString(vtsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOpptalt", Value = Convert.ToString(altKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOppt8", Value = Convert.ToString(alt8Krets) });

            //Set StatusInd as applicable
            if (!report.lenker.subrapporter.Any())
                doc.firstRapport.data.Add(new responsRapportData() { navn = "StatusInd", Value = Convert.ToString(report.StatusIndikator) });
            else
                doc.firstRapport.data.Add(new responsRapportData() { navn = "StatusInd" });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProgProStOpptalt", Value = Tools.FormatDecimal(report.opptalt.prosent) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntStBerett", Value = Convert.ToString(report.antallsb) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFrammotte", Value = Convert.ToString(report.stemmesedler.total) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProFrammotte", Value = Tools.FormatDecimal(report.frammote.prosent) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "DiffFrammFFtv", Value = "NA" });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "DiffPropFrammFFtv", Value = Tools.FormatDecimal(report.frammote.endring) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "DiffFrammFStv", Value = "NA" });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "DiffPropFrammFStv", Value = "NA" });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFhstOpptalt", Value = Convert.ToString(report.stemmesedler.fhs) });
            //doc.firstRapport.data.Add(new responsRapportData() { navn = "EvalgInkl", Value = "NA" });

            //Looping parti-liste data
            var tabell = new responsRapportTabell() { navn = "F02tabell1" };
            foreach (var item in report.partier)
            {
                var liste = new responsRapportTabellListe();
                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikode", Value = item.partiNavn.partikode });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntFhst", Value = Convert.ToString(item.stemmetall.resultat.antall.fhs) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntStemmer", Value = Convert.ToString(item.stemmetall.resultat.antall.total) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProSt", Value = Tools.FormatDecimal(item.stemmetall.resultat.prosent) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffStFFtv", Value = "NA" });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFFtv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.samme, true) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffStFStv", Value = "NA" });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFStv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.ekvivalent, true) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikategori", Value = Convert.ToString(item.partiNavn.partikategori) });
                liste.tabell = null;
                tabell.liste.Add(liste);
            }

            doc.firstRapport.tabell.Add(tabell);
            return doc;
        }

        //Fylke - progress
        public static respons GetF01(Resultatrapport report, Dictionary<string, Resultatrapport> reportCache)
        {
            //Start with blank new XML-report
            var doc = XmlHelpers.GetNewXMLReport("F01", report.tidspunkt.rapportGenerert);

            //Not using calculated StatusInd here, but same logic is applied
            var totKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr));
            var fhsKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr) &&
                Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == Resultatrapport.stemmesedler.total);
            var vtsKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr) &&
                Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == 0);
            var altKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr) &&
                Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs != Resultatrapport.stemmesedler.total);
            var alt8Kom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(report.rapportId.nr) && Resultatrapport.opptalt.endelig == 100);

            //Use StatusInd for krets count logic
            //Apply krets count to relevant selection only, extra selector needed as krets-numbers does not include fylke/kommune numbers. Use StatusInd for logic
            var kretser = reportCache.Where(Resultatrapport => Resultatrapport.Value.rapportId.nivaa == "stemmekrets" && Resultatrapport.Key.StartsWith(report.lenker.self.href)).Select(rr => rr.Value);
            var totKrets = kretser.Count();
            var fhsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 1 || Resultatrapport.StatusIndikator == 2);
            var vtsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 3 || Resultatrapport.StatusIndikator == 4);
            var altKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator >= 5);
            var alt8Krets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 8);

            //Actuall XML generation
            doc.firstRapport.data.Add(new responsRapportData() { navn = "FylkeNr", Value = report.rapportId.nr });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "FylkeNavn", Value = report.rapportId.navn });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKomm", Value = Convert.ToString(totKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommFhstOpptalt", Value = Convert.ToString(fhsKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommVtstOpptalt", Value = Convert.ToString(vtsKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommAltOpptalt", Value = Convert.ToString(altKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommAltOppt8", Value = Convert.ToString(alt8Kom) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKretser", Value = Convert.ToString(totKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserFhstOpptalt", Value = Convert.ToString(fhsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserVtstOpptalt", Value = Convert.ToString(vtsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOpptalt", Value = Convert.ToString(altKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOppt8", Value = Convert.ToString(alt8Krets) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProgProStOpptalt", Value = Tools.FormatDecimal(report.opptalt.prosent) });

            //Progress data
            var tabell = new responsRapportTabell() { navn = "F01tabell1" };
            foreach (var k in reportCache.Where(x => x.Key.StartsWith(report.lenker.self.href) && !x.Value.lenker.subrapporter.Any() && x.Value.StatusIndikator > 0 &&
                (x.Value.rapportId.nivaa == "kommune" || x.Value.rapportId.nivaa == "stemmekrets")))
            {
                //This is a change from earlier elections, stemmekrets was not orignally present fro K01/F01.
                //This is handeled in the XSLT memnu logic on the web page
                var l = new responsRapportTabellListe();
                if (k.Value.rapportId.nivaa == "stemmekrets")
                {
                    l.data.Add(new responsRapportTabellListeData()
                    {
                        navn = "KommNr",
                        Value =
                            (reportCache[k.Value.lenker.up.href].rapportId.nivaa == "bydel" ? reportCache[k.Value.lenker.up.href].lenker.up.nr : k.Value.lenker.up.nr)
                    });
                    l.data.Add(new responsRapportTabellListeData()
                    {
                        navn = "KommNavn",
                        Value =
                            (reportCache[k.Value.lenker.up.href].rapportId.nivaa == "bydel" ? reportCache[k.Value.lenker.up.href].lenker.up.navn : k.Value.lenker.up.navn)
                    });
                    l.data.Add(new responsRapportTabellListeData() { navn = "KretsNr", Value = k.Value.rapportId.nr });
                    l.data.Add(new responsRapportTabellListeData() { navn = "KretsNavn", Value = k.Value.rapportId.navn });

                }
                else
                {
                    l.data.Add(new responsRapportTabellListeData() { navn = "KommNr", Value = k.Value.rapportId.nr });
                    l.data.Add(new responsRapportTabellListeData() { navn = "KommNavn", Value = k.Value.rapportId.navn });
                    l.data.Add(new responsRapportTabellListeData() { navn = "KretsNr" });
                    l.data.Add(new responsRapportTabellListeData() { navn = "KretsNavn" });
                }
                l.data.Add(new responsRapportTabellListeData() { navn = "StatusInd", Value = Convert.ToString(k.Value.StatusIndikator) });
                tabell.liste.Add(l);
            }

            doc.firstRapport.tabell.Add(tabell);
            return doc;
        }

    }

    class XmlHelpersST
    {
        //Kommune
        public static respons GetST02(Resultatrapport report, Dictionary<string, Resultatrapport> reportCache)
        {
            //Start with blank new XML-report
            var doc = XmlHelpers.GetNewXMLReport("ST02", report.tidspunkt.rapportGenerert);

            //Apply krets count to relevant selection only, extra selector needed as krets-numbers does not include fylke/kommune numbers. Use StatusInd for logic.
            var kretser = reportCache.Where(Resultatrapport => Resultatrapport.Value.rapportId.nivaa == "stemmekrets" &&
                Resultatrapport.Key.StartsWith(report.lenker.self.href)).Select(rr => rr.Value);
            var totKrets = kretser.Count();
            var fhsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 1 || Resultatrapport.StatusIndikator == 2);
            var vtsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 3 || Resultatrapport.StatusIndikator == 4);
            var altKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator >= 5);
            var alt8Krets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 8);

            //Actuall XML generation
            doc.firstRapport.data.Add(new responsRapportData() { navn = "FylkeNavn", Value = report.lenker.up.navn });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "FylkeNr", Value = report.LegacySTRegion });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "KommNr", Value = report.rapportId.nr });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "KommNavn", Value = report.rapportId.navn });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKretser", Value = Convert.ToString(totKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserFhstOpptalt", Value = Convert.ToString(fhsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserVtstOpptalt", Value = Convert.ToString(vtsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOpptalt", Value = Convert.ToString(altKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOppt8", Value = Convert.ToString(alt8Krets) });

            //Set StatusInd as applicable
            if (!report.lenker.subrapporter.Any())
                doc.firstRapport.data.Add(new responsRapportData() { navn = "StatusInd", Value = Convert.ToString(report.StatusIndikator) });
            else
                doc.firstRapport.data.Add(new responsRapportData() { navn = "StatusInd" });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProgProStOpptalt", Value = Tools.FormatDecimal(report.opptalt.prosent) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntStBerett", Value = Convert.ToString(report.antallsb) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFrammotte", Value = Convert.ToString(report.stemmesedler.total) });

            //doc.firstRapport.data.Add(new responsRapportData() { navn = "ProFrammotte", Value = Tools.FormatDecimal(report.frammote.prosent) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProFrammotte", Value = Tools.FormatDecimal(report.frammote.prosentStemmegiving) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "DiffFrammFFtv", Value = "NA" });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "DiffPropFrammFFtv", Value = "NA" });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "DiffFrammFStv", Value = "NA" });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "DiffPropFrammFStv", Value = Tools.FormatDecimal(report.frammote.endring) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFhstOpptalt", Value = Convert.ToString(report.stemmesedler.fhs) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "EvalgInkl", Value = "NA" });

            //Looping parti-liste data
            var tabell = new responsRapportTabell() { navn = "ST02tabell1" };
            foreach (var item in report.partier)
            {
                var liste = new responsRapportTabellListe();
                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikode", Value = item.partiNavn.partikode });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntFhst", Value = Convert.ToString(item.stemmetall.resultat.antall.fhs) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntStemmer", Value = Convert.ToString(item.stemmetall.resultat.antall.total) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProSt", Value = Tools.FormatDecimal(item.stemmetall.resultat.prosent) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffStFFtv", Value = "NA" });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFFtv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.ekvivalent, true) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffStFStv", Value = "NA" });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFStv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.samme, true) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFhstFStv", Value = "NA" });
                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikategori", Value = Convert.ToString(item.partiNavn.partikategori) });
                liste.tabell = null;
                tabell.liste.Add(liste);
            }

            doc.firstRapport.tabell.Add(tabell);
            return doc;
        }

        //Krets
        public static respons GetST03(Resultatrapport report, Dictionary<string, Resultatrapport> reportCache)
        {
            //Start with blank new XML-report
            var doc = XmlHelpers.GetNewXMLReport("ST03", report.tidspunkt.rapportGenerert);

            //Determine correct aggregation parent
            var parent = reportCache[report.lenker.up.href].rapportId.nivaa == "bydel" ? reportCache[report.lenker.up.href].lenker.up : report.lenker.up;

            //Apply krets count to relevant selection only, extra selector needed as krets-numbers does not include fylke/kommune numbers. Use StatusInd for logic.
            var kretser = reportCache.Where(Resultatrapport => Resultatrapport.Value.rapportId.nivaa == "stemmekrets" &&
                Resultatrapport.Key.StartsWith(parent.href)).Select(rr => rr.Value);
            var totKrets = kretser.Count();
            var fhsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 1 || Resultatrapport.StatusIndikator == 2);
            var vtsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 3 || Resultatrapport.StatusIndikator == 4);
            var altKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator >= 5);
            var alt8Krets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 8);

            //Actuall XML generation
            doc.firstRapport.data.Add(new responsRapportData() { navn = "KommNr", Value = parent.nr });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "KommNavn", Value = parent.navn });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "FylkeNr", Value = report.LegacySTRegion });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKretser", Value = Convert.ToString(totKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserFhstOpptalt", Value = Convert.ToString(fhsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserVtstOpptalt", Value = Convert.ToString(vtsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOpptalt", Value = Convert.ToString(altKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOppt8", Value = Convert.ToString(alt8Krets) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "KretsNr", Value = report.rapportId.nr });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "KretsNavn", Value = report.rapportId.navn });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "StatusInd", Value = Convert.ToString(report.StatusIndikator) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntStBerett", Value = Convert.ToString(report.antallsb) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFrammotte", Value = Convert.ToString(report.stemmesedler.total) });

            //Use Stemmegiving for kretser, will give a reasonable value when frammøte is missing
            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProFrammotte", Value = Tools.FormatDecimal(report.frammote.prosentStemmegiving) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFhstOpptalt", Value = Convert.ToString(report.stemmesedler.fhs) });

            //Looping parti-liste data
            var tabell = new responsRapportTabell() { navn = "ST03tabell1" };
            foreach (var item in report.partier)
            {
                var liste = new responsRapportTabellListe();
                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikode", Value = item.partiNavn.partikode });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntFhst", Value = Convert.ToString(item.stemmetall.resultat.antall.fhs) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntStemmer", Value = Convert.ToString(item.stemmetall.resultat.antall.total) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProSt", Value = Tools.FormatDecimal(item.stemmetall.resultat.prosent) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffStFFtv", Value = "NA" });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFFtv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.ekvivalent, true) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffStFStv", Value = "NA" });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFStv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.samme, true) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikategori", Value = Convert.ToString(item.partiNavn.partikategori) });
                liste.tabell = null;
                tabell.liste.Add(liste);
            }

            doc.firstRapport.tabell.Add(tabell);
            return doc;
        }

        //Fylke
        public static respons GetST04(Resultatrapport report, Dictionary<string, Resultatrapport> reportCache)
        {
            //Start with blank new XML-report
            var doc = XmlHelpers.GetNewXMLReport("ST04", report.tidspunkt.rapportGenerert);

            //Misc calculations
            var maleRep = 0;
            var femaleRep = 0;

            //Mandat count from resultat/prognose
            if (report.prognose.beregnet && report.partier[0].mandattall != null)
            {
                maleRep = report.partier.SelectMany(Parti => Parti.mandattall.prognose.representanter).Count(x => x.kjonn == "M");
                femaleRep = report.partier.SelectMany(Parti => Parti.mandattall.prognose.representanter).Count(x => x.kjonn == "K");
            }
            else if (report.partier[0].mandattall != null)
            {
                maleRep = report.partier.SelectMany(Parti => Parti.mandattall.resultat.representanter).Count(x => x.kjonn == "M");
                femaleRep = report.partier.SelectMany(Parti => Parti.mandattall.resultat.representanter).Count(x => x.kjonn == "K");
            }

            //Not using calculated StatusInd here, but same logic is applied
            var totKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.LegacySTRegion == report.rapportId.nr);
            var fhsKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.LegacySTRegion == report.rapportId.nr &&
                Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == Resultatrapport.stemmesedler.total);
            var vtsKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.LegacySTRegion == report.rapportId.nr &&
                Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == 0);
            var altKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.LegacySTRegion == report.rapportId.nr &&
                Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs != Resultatrapport.stemmesedler.total);
            var alt8Kom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.LegacySTRegion == report.rapportId.nr &&
                Resultatrapport.opptalt.endelig == 100);

            //Apply krets count to relevant selection only, extra selector needed as krets-numbers does not include fylke/kommune numbers. Use StatusInd for logic
            var kretser = reportCache.Where(Resultatrapport => Resultatrapport.Value.rapportId.nivaa == "stemmekrets" && Resultatrapport.Key.StartsWith(report.lenker.self.href)).Select(rr => rr.Value);
            var totKrets = kretser.Count();
            var fhsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 1 || Resultatrapport.StatusIndikator == 2);
            var vtsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 3 || Resultatrapport.StatusIndikator == 4);
            var altKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator >= 5);
            var alt8Krets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 8);

            //Conditional indicators, sistemandat
            double? tapeStemmer = null;
            double? vinneStemmer = null;
            string tapeParti = string.Empty;
            string vinneParti = string.Empty;

            //Calculate closest win/lose mandat. Numbers are no longer posssible from ST-2021. We kan only do +/- indicators
            if (report.prognose.beregnet && report.partier[0].mandattall != null)
            {
                tapeStemmer = report.partier.Select(Parti => Parti.mandattall.prognose.sisteMandat.kvotient).Min();
                vinneStemmer = report.partier.Select(Parti => Parti.mandattall.prognose.nesteMandat.kvotient).Max();

                tapeParti = report.partier.First(x => x.mandattall.prognose.sisteMandat.kvotient == tapeStemmer).partiNavn.partikode;
                vinneParti = report.partier.First(x => x.mandattall.prognose.nesteMandat.kvotient == vinneStemmer).partiNavn.partikode;
            }
            else if (report.partier[0].mandattall != null)
            {
                tapeStemmer = report.partier.Select(Parti => Parti.mandattall.resultat.sisteMandat.kvotient).Min();
                vinneStemmer = report.partier.Select(Parti => Parti.mandattall.resultat.nesteMandat.kvotient).Max();

                tapeParti = report.partier.First(x => x.mandattall.resultat.sisteMandat.kvotient == tapeStemmer).partiNavn.partikode;
                vinneParti = report.partier.First(x => x.mandattall.resultat.nesteMandat.kvotient == vinneStemmer).partiNavn.partikode;
            }

            //Actuall XML generation
            doc.firstRapport.data.Add(new responsRapportData() { navn = "FylkeNr", Value = report.rapportId.nr });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "FylkeNavn", Value = report.rapportId.navn });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKomm", Value = Convert.ToString(totKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommFhstOpptalt", Value = Convert.ToString(fhsKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommVtstOpptalt", Value = Convert.ToString(vtsKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommAltOpptalt", Value = Convert.ToString(altKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommAltOppt8", Value = Convert.ToString(alt8Kom) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKretser", Value = Convert.ToString(totKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserFhstOpptalt", Value = Convert.ToString(fhsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserVtstOpptalt", Value = Convert.ToString(vtsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOpptalt", Value = Convert.ToString(altKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOppt8", Value = Convert.ToString(alt8Krets) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "StDevFylke", Value = (report.prognose.beregnet ? Tools.FormatDecimal(report.prognose.historiskTotalavvik) : "0") });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProgProStOpptalt", Value = Tools.FormatDecimal(report.opptalt.prosent) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntStBerett", Value = Convert.ToString(report.antallsb) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFrammotte", Value = Convert.ToString(report.stemmesedler.total) });

            //doc.firstRapport.data.Add(new responsRapportData() { navn = "ProFrammotte", Value = Tools.FormatDecimal(report.frammote.prosent) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProFrammotte", Value = Tools.FormatDecimal(report.frammote.prosentStemmegiving) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFhstOpptalt", Value = Convert.ToString(report.stemmesedler.fhs) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "PartikodeMiste", Value = tapeParti });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntStMiste", Value = "" });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "PartikodeVinne", Value = vinneParti });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntStVinne", Value = "" });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntRepKvinner", Value = Convert.ToString(femaleRep) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntRepMenn", Value = Convert.ToString(maleRep) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntRepUkjKjonn", Value = "0" });

            //Looping parti-liste data
            var tabell = new responsRapportTabell() { navn = "ST04tabell1" };
            foreach (var item in report.partier)
            {
                var liste = new responsRapportTabellListe();
                //Opptalt data
                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikode", Value = item.partiNavn.partikode });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntFhst", Value = Convert.ToString(item.stemmetall.resultat.antall.fhs) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntStemmer", Value = Convert.ToString(item.stemmetall.resultat.antall.total) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProSt", Value = Tools.FormatDecimal(item.stemmetall.resultat.prosent) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFFtv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.ekvivalent, true) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFStv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.samme, true) });

                //Prognosetall if avail
                if (report.prognose.beregnet)
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgProSt", Value = Tools.FormatDecimal(item.stemmetall.prognose.prosent) });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFFtv", Value = Tools.FormatDecimal(item.stemmetall.prognose.endring.ekvivalent, true) });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFStv", Value = Tools.FormatDecimal(item.stemmetall.prognose.endring.samme, true) });
                }
                else
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgProSt", Value = "-" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFFtv", Value = "-" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFStv", Value = "-" });
                }

                //Mandat-tall either from prognose or real segment
                if (report.prognose.beregnet && report.partier[0].mandattall != null)
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStv", Value = Convert.ToString(item.mandattall.prognose.antall) });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndtFStv", Value = Tools.AddSign(item.mandattall.prognose.endring) });

                    //Win/Loose indicator flags, sistemandat
                    if (item.mandattall.prognose.sisteMandat.kvotient == tapeStemmer)
                        liste.data.Add(new responsRapportTabellListeData() { navn = "ProgVinneMisteStvInd", Value = "-" });
                    else if (item.mandattall.prognose.nesteMandat.kvotient == vinneStemmer)
                        liste.data.Add(new responsRapportTabellListeData() { navn = "ProgVinneMisteStvInd", Value = "+" });
                    else
                        liste.data.Add(new responsRapportTabellListeData() { navn = "ProgVinneMisteStvInd" });

                    //From ST-2021 numbers for StVinne/StMiste are no longer possible - emtpy placeholders for format compatibility only
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntStMiste", Value = "" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgPropAntStMiste", Value = "" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntStVinne", Value = "" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgPropAntStVinne", Value = "" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgSisteMndtRang", Value = Convert.ToString(item.mandattall.prognose.sisteMandat.mandatrang) });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgUtjMndtStv", Value = Convert.ToString(item.mandattall.prognose.utjevningAntall) });
                }
                else if (report.partier[0].mandattall != null)
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStv", Value = Convert.ToString(item.mandattall.resultat.antall) });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndtFStv", Value = Tools.AddSign(item.mandattall.resultat.endring) });

                    //Win/Loose indicator flags, sistemandat
                    if (item.mandattall.resultat.sisteMandat.kvotient == tapeStemmer)
                        liste.data.Add(new responsRapportTabellListeData() { navn = "ProgVinneMisteStvInd", Value = "-" });
                    else if (item.mandattall.resultat.nesteMandat.kvotient == vinneStemmer)
                        liste.data.Add(new responsRapportTabellListeData() { navn = "ProgVinneMisteStvInd", Value = "+" });
                    else
                        liste.data.Add(new responsRapportTabellListeData() { navn = "ProgVinneMisteStvInd" });

                    //From ST-2021 numbers for StVinne/StMiste are no longer possible - emtpy placeholders for format compatibility only
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntStMiste", Value = "" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgPropAntStMiste", Value = "" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntStVinne", Value = "" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgPropAntStVinne", Value = "" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgSisteMndtRang", Value = Convert.ToString(item.mandattall.resultat.sisteMandat.mandatrang) });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgUtjMndtStv", Value = Convert.ToString(item.mandattall.resultat.utjevningAntall) });
                }
                else
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStv", Value = "0" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndtFStv", Value = "0" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgVinneMisteStvInd" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntStMiste", Value = "" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgPropAntStMiste", Value = "" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntStVinne", Value = "" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgPropAntStVinne", Value = "" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgSisteMndtRang", Value = "" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgUtjMndtStv", Value = "0" });

                }

                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikategori", Value = Convert.ToString(item.partiNavn.partikategori) });
                liste.tabell = null;
                tabell.liste.Add(liste);
            }

            doc.firstRapport.tabell.Add(tabell);

            //Representater
            tabell = new responsRapportTabell() { navn = "ST04tabell2" };
            if (report.partier[0].mandattall != null)
            {
                //Switch mandatliste on resultat/prognose 
                IEnumerable<Parti> reps = new List<Parti>();
                if (report.prognose.beregnet)
                {
                    reps = report.partier.Where(x => x.mandattall.prognose.representanter.Any());
                }
                else if (report.partier[0].mandattall != null)
                {
                    reps = report.partier.Where(x => x.mandattall.resultat.representanter.Any());
                }

                //Actuall party loop based on correct array
                foreach (var p in reps)
                {
                    //Parti container
                    var liste = new responsRapportTabellListe();
                    liste.data.Add(new responsRapportTabellListeData() { navn = "PartikodeRep", Value = p.partiNavn.partikode });

                    //Actuall list of candidates
                    liste.tabell.navn = "ST04tabell21";

                    foreach (var r in p.mandattall.resultat.representanter)
                    {
                        var lrp = new responsRapportTabellListeTabellListe();
                        lrp.data.Add(new responsRapportTabellListeTabellListeData() { navn = "RepNavn", Value = r.navn });
                        lrp.data.Add(new responsRapportTabellListeTabellListeData() { navn = "RepKjonn", Value = r.kjonn });
                        lrp.data.Add(new responsRapportTabellListeTabellListeData() { navn = "RepAlder", Value = Convert.ToString(r.alder) });
                        lrp.data.Add(new responsRapportTabellListeTabellListeData() { navn = "RepBoKommuneNr", Value = "NA" });
                        lrp.data.Add(new responsRapportTabellListeTabellListeData() { navn = "RepBoKommuneNavn", Value = "NA" });
                        lrp.data.Add(new responsRapportTabellListeTabellListeData() { navn = "RepUtjInd", Value = Convert.ToString(r.utjevningsmandat) });

                        liste.tabell.liste.Add(lrp);
                    }

                    tabell.liste.Add(liste);
                }
            }

            doc.firstRapport.tabell.Add(tabell);

            //Progress data
            tabell = new responsRapportTabell() { navn = "ST04tabell3" };
            foreach (var k in reportCache.Where(x => x.Key.StartsWith(report.lenker.self.href) && !x.Value.lenker.subrapporter.Any() && x.Value.StatusIndikator > 0 &&
                (x.Value.rapportId.nivaa == "kommune" || x.Value.rapportId.nivaa == "stemmekrets")))
            {
                var l = new responsRapportTabellListe();
                if (k.Value.rapportId.nivaa == "stemmekrets")
                {
                    l.data.Add(new responsRapportTabellListeData()
                    {
                        navn = "KommNr",
                        Value =
                            (reportCache[k.Value.lenker.up.href].rapportId.nivaa == "bydel" ? reportCache[k.Value.lenker.up.href].lenker.up.nr : k.Value.lenker.up.nr)
                    });
                    l.data.Add(new responsRapportTabellListeData()
                    {
                        navn = "KommNavn",
                        Value =
                            (reportCache[k.Value.lenker.up.href].rapportId.nivaa == "bydel" ? reportCache[k.Value.lenker.up.href].lenker.up.navn : k.Value.lenker.up.navn)
                    });
                    l.data.Add(new responsRapportTabellListeData() { navn = "KretsNr", Value = k.Value.rapportId.nr });
                    l.data.Add(new responsRapportTabellListeData() { navn = "KretsNavn", Value = k.Value.rapportId.navn });

                }
                else
                {
                    l.data.Add(new responsRapportTabellListeData() { navn = "KommNr", Value = k.Value.rapportId.nr });
                    l.data.Add(new responsRapportTabellListeData() { navn = "KommNavn", Value = k.Value.rapportId.navn });
                    l.data.Add(new responsRapportTabellListeData() { navn = "KretsNr" });
                    l.data.Add(new responsRapportTabellListeData() { navn = "KretsNavn" });
                }
                l.data.Add(new responsRapportTabellListeData() { navn = "StatusInd", Value = Convert.ToString(k.Value.StatusIndikator) });
                tabell.liste.Add(l);
            }

            doc.firstRapport.tabell.Add(tabell);

            return doc;
        }

        //Bydel (Oslo)
        public static respons GetST05(Resultatrapport report, Dictionary<string, Resultatrapport> reportCache)
        {
            //Start with blank new XML-report
            var doc = XmlHelpers.GetNewXMLReport("ST05", report.tidspunkt.rapportGenerert);

            //Apply krets count to relevant selection only, extra selector needed as krets-numbers does not include fylke/kommune numbers. Use StatusInd for logic.
            var kretser = reportCache.Where(Resultatrapport => Resultatrapport.Value.rapportId.nivaa == "stemmekrets" &&
                Resultatrapport.Value.lenker.up.href == report.lenker.self.href).Select(rr => rr.Value);
            var totKrets = kretser.Count();
            var fhsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 1 || Resultatrapport.StatusIndikator == 2);
            var vtsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 3 || Resultatrapport.StatusIndikator == 4);
            var altKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator >= 5);
            var alt8Krets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 8);

            //Actuall XML generation
            doc.firstRapport.data.Add(new responsRapportData() { navn = "KommNr", Value = report.lenker.up.nr });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "KommNavn", Value = report.lenker.up.navn });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "BydelNr", Value = report.rapportId.nr });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "BydelNavn", Value = report.rapportId.navn });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKretser", Value = Convert.ToString(totKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserFhstOpptalt", Value = Convert.ToString(fhsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserVtstOpptalt", Value = Convert.ToString(vtsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOpptalt", Value = Convert.ToString(altKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOppt8", Value = Convert.ToString(alt8Krets) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntStBerett", Value = Convert.ToString(report.antallsb) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFrammotte", Value = Convert.ToString(report.stemmesedler.total) });

            //Use Stemmegiving for bydeler, will give a reasonable value when frammøte is missing
            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProFrammotte", Value = Tools.FormatDecimal(report.frammote.prosentStemmegiving) });

            //Looping parti-liste data
            var tabell = new responsRapportTabell() { navn = "ST05tabell1" };
            foreach (var item in report.partier)
            {
                var liste = new responsRapportTabellListe();
                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikode", Value = item.partiNavn.partikode });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntStemmer", Value = Convert.ToString(item.stemmetall.resultat.antall.total) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProSt", Value = Tools.FormatDecimal(item.stemmetall.resultat.prosent) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFKsv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.ekvivalent, true) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFStv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.samme, true) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikategori", Value = Convert.ToString(item.partiNavn.partikategori) });
                liste.tabell = null;
                tabell.liste.Add(liste);
            }

            doc.firstRapport.tabell.Add(tabell);
            return doc;
        }

        //Land
        public static respons GetST06(Resultatrapport report, Dictionary<string, Resultatrapport> reportCache)
        {
            //Start with blank new XML-report
            var doc = XmlHelpers.GetNewXMLReport("ST06", report.tidspunkt.rapportGenerert);

            //Misc calculations
            var maleRep = 0;
            var femaleRep = 0;

            //Switch mandattall on resultat/prognose 
            maleRep = reportCache.Values.Where(Resultatrapport => Resultatrapport.rapportId.nivaa == "fylke" && 
                Resultatrapport.prognose.beregnet && Resultatrapport.partier[0].mandattall != null)
                .SelectMany(Resultatrapport => Resultatrapport.partier)
                .SelectMany(Parti => Parti.mandattall.prognose.representanter).Count(x => x.kjonn == "M");

            femaleRep = reportCache.Values.Where(Resultatrapport => Resultatrapport.rapportId.nivaa == "fylke" &&
                Resultatrapport.prognose.beregnet && Resultatrapport.partier[0].mandattall != null)
                .SelectMany(Resultatrapport => Resultatrapport.partier)
                .SelectMany(Parti => Parti.mandattall.prognose.representanter).Count(x => x.kjonn == "K");

            maleRep += reportCache.Values.Where(Resultatrapport => Resultatrapport.rapportId.nivaa == "fylke" &&
                 ! Resultatrapport.prognose.beregnet && Resultatrapport.partier[0].mandattall != null)
                .SelectMany(Resultatrapport => Resultatrapport.partier)
                .SelectMany(Parti => Parti.mandattall.resultat.representanter).Count(x => x.kjonn == "M");

            femaleRep += reportCache.Values.Where(Resultatrapport => Resultatrapport.rapportId.nivaa == "fylke" &&
                ! Resultatrapport.prognose.beregnet && Resultatrapport.partier[0].mandattall != null)
                .SelectMany(Resultatrapport => Resultatrapport.partier)
                .SelectMany(Parti => Parti.mandattall.resultat.representanter).Count(x => x.kjonn == "K");

            //Not using calculated StatusInd here, but same logic is applied
            var totKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune");
            var fhsKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" &&
                Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == Resultatrapport.stemmesedler.total);
            var vtsKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" &&
                Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == 0);
            var altKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" &&
                Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs != Resultatrapport.stemmesedler.total);
            var alt8Kom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.opptalt.endelig == 100);

            //Use StatusInd for krets count logic
            var totKrets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets");
            var fhsKrets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && (Resultatrapport.StatusIndikator == 1 || Resultatrapport.StatusIndikator == 2));
            var vtsKrets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && (Resultatrapport.StatusIndikator == 3 || Resultatrapport.StatusIndikator == 4));
            var altKrets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && Resultatrapport.StatusIndikator >= 5);
            var alt8Krets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && Resultatrapport.StatusIndikator == 8);

            //From ST-2021 number for utjevning are gone and we can only do indicator for closest loose for utjevning
            double? tapeStemmer = null;
            if (report.prognose.beregnet && report.partier[0].mandattall != null)
            {
                tapeStemmer = report.partier.Select(Parti => Parti.mandattall.prognose.utjevning.kvotient).Min();
            }
            else if (report.partier[0].mandattall != null)
            {
                tapeStemmer = report.partier.Select(Parti => Parti.mandattall.resultat.utjevning.kvotient).Min();
            }

            //Actuall XML generation
            doc.firstRapport.data.Add(new responsRapportData() { navn = "StDevLand", Value = (report.prognose.beregnet ? Tools.FormatDecimal(report.prognose.historiskTotalavvik) : "0") });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKomm", Value = Convert.ToString(totKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommFhstOpptalt", Value = Convert.ToString(fhsKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommVtstOpptalt", Value = Convert.ToString(vtsKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommAltOpptalt", Value = Convert.ToString(altKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommAltOppt8", Value = Convert.ToString(alt8Kom) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKretser", Value = Convert.ToString(totKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserFhstOpptalt", Value = Convert.ToString(fhsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserVtstOpptalt", Value = Convert.ToString(vtsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOpptalt", Value = Convert.ToString(altKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOppt8", Value = Convert.ToString(alt8Krets) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProgProStOpptalt", Value = Tools.FormatDecimal(report.opptalt.prosent) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntStBerett", Value = Convert.ToString(report.antallsb) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFrammotte", Value = Convert.ToString(report.stemmesedler.total) });

            //doc.firstRapport.data.Add(new responsRapportData() { navn = "ProFrammotte", Value = Tools.FormatDecimal(report.frammote.prosent) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProFrammotte", Value = Tools.FormatDecimal(report.frammote.prosentStemmegiving) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "DiffPropFrammFStv", Value = Tools.FormatDecimal(report.frammote.endring, true) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFhstOpptalt", Value = Convert.ToString(report.stemmesedler.fhs) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntRepKvinner", Value = Convert.ToString(femaleRep) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntRepMenn", Value = Convert.ToString(maleRep) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntRepUkjKjonn", Value = "0" });

            //Looping parti-liste data
            var tabell = new responsRapportTabell() { navn = "ST06tabell1" };
            foreach (var item in report.partier)
            {
                var liste = new responsRapportTabellListe();
                //Opptalt data7
                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikode", Value = item.partiNavn.partikode });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntFhst", Value = Convert.ToString(item.stemmetall.resultat.antall.fhs) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntStemmer", Value = Convert.ToString(item.stemmetall.resultat.antall.total) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProSt", Value = Tools.FormatDecimal(item.stemmetall.resultat.prosent, false, "F2") });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFFtv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.ekvivalent, true) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFStv", Value = Tools.FormatDecimal(item.stemmetall.resultat.endring.samme, true) });

                //Prognosetall if avail
                if (report.prognose.beregnet)
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgProSt", Value = Tools.FormatDecimal(item.stemmetall.prognose.prosent,false,"F2") });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFFtv", Value = Tools.FormatDecimal(item.stemmetall.prognose.endring.ekvivalent, true) });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFStv", Value = Tools.FormatDecimal(item.stemmetall.prognose.endring.samme, true) });
                }
                else
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgProSt", Value = "-" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFFtv", Value = "-" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgPropFStv", Value = "-" });
                }

                //Mandat-tall either from prognose or real segment
                if (report.prognose.beregnet && report.partier[0].mandattall != null)
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStv", Value = Convert.ToString(item.mandattall.prognose.antall) });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndtFStv", Value = Tools.AddSign(item.mandattall.prognose.endring) });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntUtjMndtStv", Value = Convert.ToString(item.mandattall.prognose.utjevningAntall) });

                    //Win/Loose indicator flags, utjevningsmandat
                    if (item.mandattall.prognose.utjevning.kvotient == tapeStemmer)
                        liste.data.Add(new responsRapportTabellListeData() { navn = "ProgVinneMisteUtjInd", Value = "-" });
                    else
                        liste.data.Add(new responsRapportTabellListeData() { navn = "ProgVinneMisteUtjInd" });
                }
                else if (report.partier[0].mandattall != null)
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStv", Value = Convert.ToString(item.mandattall.resultat.antall) });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndtFStv", Value = Tools.AddSign(item.mandattall.resultat.endring) });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntUtjMndtStv", Value = Convert.ToString(item.mandattall.resultat.utjevningAntall) });

                    //Win/Loose indicator flags, utjevningsmandat
                    if (item.mandattall.resultat.utjevning.kvotient == tapeStemmer)
                        liste.data.Add(new responsRapportTabellListeData() { navn = "ProgVinneMisteUtjInd", Value = "-" });
                    else
                        liste.data.Add(new responsRapportTabellListeData() { navn = "ProgVinneMisteUtjInd" });
                }
                else
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStv", Value = "0" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "DiffProgAntMndtFStv", Value = "0" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntUtjMndtStv", Value = "0" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgVinneMisteUtjInd" });
                }

                // From ST2021 numbers for StVinne/Miste Utj are not longer available. Empty placeholders for format compatibility only
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntStMisteUtj", Value = "" });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProgPropAntStMisteUtj", Value = "" });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntStVinneUtj", Value = "" });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProgPropAntStVinneUtj", Value = "" });

                liste.data.Add(new responsRapportTabellListeData() { navn = "Partikategori", Value = Convert.ToString(item.partiNavn.partikategori) });
                liste.tabell = null;
                tabell.liste.Add(liste);
            }

            doc.firstRapport.tabell.Add(tabell);
            return doc;

        }

        //Land - Stortingsoversikt/prognose
        public static respons GetST07(Dictionary<string, Resultatrapport> reportCache, string rootRef)
        {
            //Start with blank new XML-report based on root
            var doc = XmlHelpers.GetNewXMLReport("ST07", reportCache[rootRef].tidspunkt.rapportGenerert);
            doc.firstRapport.data.Add(new responsRapportData() { navn = "StDevLand", Value = (reportCache[rootRef].prognose.beregnet ? Tools.FormatDecimal(reportCache[rootRef].prognose.historiskTotalavvik) : "0") });

            //Fylkesliste based on fylkesrapporter
            var tabell = new responsRapportTabell() { navn = "ST07tabell1" };
            foreach (var f in reportCache.Where(x => x.Value.rapportId.nivaa == "fylke").Select(r => r.Value))
            {
                //Per fylke calculations    
                //Not using calculated StatusInd here, but same logic is applied
                var totKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(f.rapportId.nr));
                var fhsKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(f.rapportId.nr) &&
                    Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == Resultatrapport.stemmesedler.total);
                var vtsKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(f.rapportId.nr) &&
                    Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == 0);
                var altKom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(f.rapportId.nr) &&
                    Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs != Resultatrapport.stemmesedler.total);
                var alt8Kom = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune" && Resultatrapport.rapportId.nr.StartsWith(f.rapportId.nr) &&
                    Resultatrapport.opptalt.endelig == 100);

                //Apply krets count to relevant selection only, extra selector needed as krets-numbers does not include fylke/kommune numbers. Use StatusInd for logic
                var kretser = reportCache.Where(Resultatrapport => Resultatrapport.Value.rapportId.nivaa == "stemmekrets" && Resultatrapport.Key.StartsWith(f.lenker.self.href)).Select(rr => rr.Value);
                var totKrets = kretser.Count();
                var fhsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 1 || Resultatrapport.StatusIndikator == 2);
                var vtsKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 3 || Resultatrapport.StatusIndikator == 4);
                var altKrets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator >= 5);
                var alt8Krets = kretser.Count(Resultatrapport => Resultatrapport.StatusIndikator == 8);

                //Conditional indicators, sistemandat
                double? tapeStemmer = null;
                double? vinneStemmer = null;
                string tapeParti = string.Empty;
                string vinneParti = string.Empty;

                //Calculate closest win/lose mandat. Numbers are no longer posssible from ST-2021. We kan only do +/- indicators
                if (f.prognose.beregnet && f.partier[0].mandattall != null)
                {
                    tapeStemmer = f.partier.Select(Parti => Parti.mandattall.prognose.sisteMandat.kvotient).Min();
                    vinneStemmer = f.partier.Select(Parti => Parti.mandattall.prognose.nesteMandat.kvotient).Max();

                    tapeParti = f.partier.First(x => x.mandattall.prognose.sisteMandat.kvotient == tapeStemmer).partiNavn.partikode;
                    vinneParti = f.partier.First(x => x.mandattall.prognose.nesteMandat.kvotient == vinneStemmer).partiNavn.partikode;
                }
                else if (f.partier[0].mandattall != null)
                {
                    tapeStemmer = f.partier.Select(Parti => Parti.mandattall.resultat.sisteMandat.kvotient).Min();
                    vinneStemmer = f.partier.Select(Parti => Parti.mandattall.resultat.nesteMandat.kvotient).Max();

                    tapeParti = f.partier.First(x => x.mandattall.resultat.sisteMandat.kvotient == tapeStemmer).partiNavn.partikode;
                    vinneParti = f.partier.First(x => x.mandattall.resultat.nesteMandat.kvotient == vinneStemmer).partiNavn.partikode;
                }

                var liste = new responsRapportTabellListe();
                liste.data.Add(new responsRapportTabellListeData() { navn = "FylkeNr", Value = f.rapportId.nr });
                liste.data.Add(new responsRapportTabellListeData() { navn = "FylkeNavn", Value = f.rapportId.navn });

                liste.data.Add(new responsRapportTabellListeData() { navn = "TotAntKomm", Value = Convert.ToString(totKom) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntKommFhstOpptalt", Value = Convert.ToString(fhsKom) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntKommVtstOpptalt", Value = Convert.ToString(vtsKom) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntKommAltOpptalt", Value = Convert.ToString(altKom) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntKommAltOppt8", Value = Convert.ToString(alt8Kom) });

                liste.data.Add(new responsRapportTabellListeData() { navn = "TotAntKretser", Value = Convert.ToString(totKrets) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntKretserFhstOpptalt", Value = Convert.ToString(fhsKrets) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntKretserVtstOpptalt", Value = Convert.ToString(vtsKrets) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntKretserAltOpptalt", Value = Convert.ToString(altKrets) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntKretserAltOppt8", Value = Convert.ToString(alt8Krets) });

                liste.data.Add(new responsRapportTabellListeData() { navn = "AntStBerett", Value = Convert.ToString(f.antallsb) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntFrammotte", Value = Convert.ToString(f.stemmesedler.total) });

                //liste.data.Add(new responsRapportTabellListeData() { navn = "ProFrammotte", Value = Tools.FormatDecimal(f.frammote.prosent) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProFrammotte", Value = Tools.FormatDecimal(f.frammote.prosentStemmegiving) });

                liste.data.Add(new responsRapportTabellListeData() { navn = "AntFhstOpptalt", Value = Convert.ToString(f.stemmesedler.fhs) });

                liste.data.Add(new responsRapportTabellListeData() { navn = "StDevFylke", Value = (f.prognose.beregnet ? Tools.FormatDecimal(f.prognose.historiskTotalavvik) : "0") });

                liste.data.Add(new responsRapportTabellListeData() { navn = "ProgProStOpptalt", Value = Tools.FormatDecimal(f.opptalt.prosent) });

                liste.data.Add(new responsRapportTabellListeData() { navn = "PartikodeMiste", Value = tapeParti });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntStMiste", Value = "" });
                liste.data.Add(new responsRapportTabellListeData() { navn = "PartikodeVinne", Value = vinneParti });
                liste.data.Add(new responsRapportTabellListeData() { navn = "AntStVinne", Value = "" });

                //Loop partier per fylke
                liste.tabell = new responsRapportTabellListeTabell() { navn = "ST07tabell11" };
                foreach (var p in f.partier)
                {
                    var liste2 = new responsRapportTabellListeTabellListe();
                    liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "Partikode", Value = p.partiNavn.partikode });
                    if (f.prognose.beregnet && f.partier[0].mandattall != null)
                    {
                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgAntMndtStv", Value = Convert.ToString(p.mandattall.prognose.antall) });
                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "DiffProgAntMndtFStv", Value = Tools.AddSign(p.mandattall.prognose.endring) });

                        //Win/Loose indicator flags, sistemandat
                        if (p.mandattall.prognose.sisteMandat.kvotient == tapeStemmer)
                            liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgVinneMisteStvInd", Value = "-" });
                        else if (p.mandattall.prognose.nesteMandat.kvotient == vinneStemmer)
                            liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgVinneMisteStvInd", Value = "+" });
                        else
                            liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgVinneMisteStvInd" });

                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgUtjMndtStv", Value = Convert.ToString(p.mandattall.prognose.utjevningAntall) });
                    }
                    else if (f.partier[0].mandattall != null)
                    {
                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgAntMndtStv", Value = Convert.ToString(p.mandattall.resultat.antall) });
                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "DiffProgAntMndtFStv", Value = Tools.AddSign(p.mandattall.resultat.endring) });

                        //Win/Loose indicator flags, sistemandat
                        if (p.mandattall.resultat.sisteMandat.kvotient == tapeStemmer)
                            liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgVinneMisteStvInd", Value = "-" });
                        else if (p.mandattall.resultat.nesteMandat.kvotient == vinneStemmer)
                            liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgVinneMisteStvInd", Value = "+" });
                        else
                            liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgVinneMisteStvInd" });

                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgUtjMndtStv", Value = Convert.ToString(p.mandattall.resultat.utjevningAntall) });
                    }
                    else 
                    {
                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgAntMndtStv" });
                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "DiffProgAntMndtFStv" });
                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgVinneMisteStvInd" });
                        liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "ProgUtjMndtStv" });
                    }
                    liste2.data.Add(new responsRapportTabellListeTabellListeData() { navn = "Partikategori", Value = Convert.ToString(p.partiNavn.partikategori) });
                    liste.tabell.liste.Add(liste2);
                }
                tabell.liste.Add(liste);
            }
            doc.firstRapport.tabell.Add(tabell);

            //Mandater per parti based on st-root
            tabell = new responsRapportTabell() { navn = "ST07tabell2" };
            foreach (var m in reportCache[rootRef].partier)
            {
                var liste = new responsRapportTabellListe();
                liste.data.Add(new responsRapportTabellListeData() { navn = "PartikodeL", Value = m.partiNavn.partikode });

                //Pick numbers from prognose or resultat
                if (reportCache[rootRef].prognose.beregnet && m.mandattall != null)
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntDMndtStvL", Value = Convert.ToString(m.mandattall.prognose.antall - m.mandattall.prognose.utjevningAntall) });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntUMndtStvL", Value = Convert.ToString(m.mandattall.prognose.utjevningAntall) });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStvL", Value = Convert.ToString(m.mandattall.prognose.antall) });
                }
                else if (m.mandattall != null)
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntDMndtStvL", Value = Convert.ToString(m.mandattall.resultat.antall - m.mandattall.resultat.utjevningAntall) });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntUMndtStvL", Value = Convert.ToString(m.mandattall.resultat.utjevningAntall) });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStvL", Value = Convert.ToString(m.mandattall.resultat.antall) });
                }
                else
                {
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntDMndtStvL" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntUMndtStvL" });
                    liste.data.Add(new responsRapportTabellListeData() { navn = "ProgAntMndtStvL" });
                }

                liste.data.Add(new responsRapportTabellListeData() { navn = "PartikategoriL", Value = Convert.ToString(m.partiNavn.partikategori) });
                liste.tabell = null;
                tabell.liste.Add(liste);
            }
            doc.firstRapport.tabell.Add(tabell);
            return doc;
        }

        //Land - Frammøte topp/bunn 10
        public static respons GetST14(Dictionary<string, Resultatrapport> reportCache, string rootRef)
        {
            //Start with blank new XML-report
            var doc = XmlHelpers.GetNewXMLReport("ST14", reportCache[rootRef].tidspunkt.rapportGenerert);

            //Misc calculations
            //Not using calculated StatusInd here, but same logic is applied
            var komListe = reportCache.Values.Where(Resultatrapport => Resultatrapport.rapportId.nivaa == "kommune");
            var totKom = komListe.Count();
            var fhsKom = komListe.Count(Resultatrapport => Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == Resultatrapport.stemmesedler.total);
            var vtsKom = komListe.Count(Resultatrapport => Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs == 0);
            var altKom = komListe.Count(Resultatrapport => Resultatrapport.stemmesedler.total > 0 && Resultatrapport.stemmesedler.fhs != Resultatrapport.stemmesedler.total);
            var alt8Kom = komListe.Count(Resultatrapport => Resultatrapport.opptalt.endelig == 100);

            //Use StatusInd for krets count logic
            var totKrets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets");
            var fhsKrets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && (Resultatrapport.StatusIndikator == 1 || Resultatrapport.StatusIndikator == 2));
            var vtsKrets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && (Resultatrapport.StatusIndikator == 3 || Resultatrapport.StatusIndikator == 4));
            var altKrets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && Resultatrapport.StatusIndikator >= 5);
            var alt8Krets = reportCache.Values.Count(Resultatrapport => Resultatrapport.rapportId.nivaa == "stemmekrets" && Resultatrapport.StatusIndikator == 8);

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKomm", Value = Convert.ToString(totKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommFhstOpptalt", Value = Convert.ToString(fhsKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommVtstOpptalt", Value = Convert.ToString(vtsKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommAltOpptalt", Value = Convert.ToString(altKom) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKommAltOppt8", Value = Convert.ToString(alt8Kom) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "TotAntKretser", Value = Convert.ToString(totKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserFhstOpptalt", Value = Convert.ToString(fhsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserVtstOpptalt", Value = Convert.ToString(vtsKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOpptalt", Value = Convert.ToString(altKrets) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntKretserAltOppt8", Value = Convert.ToString(alt8Krets) });

            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntStBerett", Value = Convert.ToString(reportCache[rootRef].antallsb) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "AntFrammotte", Value = Convert.ToString(reportCache[rootRef].stemmesedler.total) });

            //doc.firstRapport.data.Add(new responsRapportData() { navn = "ProFrammotte", Value = Tools.FormatDecimal(reportCache[rootRef].frammote.prosent) });
            doc.firstRapport.data.Add(new responsRapportData() { navn = "ProFrammotte", Value = Tools.FormatDecimal(reportCache[rootRef].frammote.prosentStemmegiving) });

            //Top 10
            var tabell = new responsRapportTabell() { navn = "ST14tabell1" };
            foreach (var item in komListe.Where(x => x.frammote.prosent.HasValue).OrderByDescending(y => y.frammote.prosentStemmegiving).Take(10))
            {
                var liste = new responsRapportTabellListe();

                liste.data.Add(new responsRapportTabellListeData() { navn = "KommNavnHoy", Value = item.lenker.self.navn });
                liste.data.Add(new responsRapportTabellListeData() { navn = "FylkeNavnHoy", Value = item.lenker.up.navn });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProFrammotteHoy", Value = Tools.FormatDecimal(item.frammote.prosentStemmegiving) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFrammFFtvHoy", Value = "-" });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFrammFStvHoy", Value = Tools.FormatDecimal(item.frammote.endring, true) });
                liste.tabell = null;
                tabell.liste.Add(liste);
            }
            doc.firstRapport.tabell.Add(tabell);

            //Bottom 10
            tabell = new responsRapportTabell() { navn = "ST14tabell2" };
            foreach (var item in komListe.Where(x => x.frammote.prosent.HasValue).OrderBy(y => y.frammote.prosentStemmegiving).Take(10))
            {
                var liste = new responsRapportTabellListe();

                liste.data.Add(new responsRapportTabellListeData() { navn = "KommNavnLav", Value = item.lenker.self.navn });
                liste.data.Add(new responsRapportTabellListeData() { navn = "FylkeNavnLav", Value = item.lenker.up.navn });
                liste.data.Add(new responsRapportTabellListeData() { navn = "ProFrammotteLav", Value = Tools.FormatDecimal(item.frammote.prosentStemmegiving) });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFrammFFtvLav", Value = "-" });
                liste.data.Add(new responsRapportTabellListeData() { navn = "DiffPropFrammFStvLav", Value = Tools.FormatDecimal(item.frammote.endring, true) });
                liste.tabell = null;
                tabell.liste.Add(liste);
            }
            doc.firstRapport.tabell.Add(tabell);
            return doc;

        }


    }
}
